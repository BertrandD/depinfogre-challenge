<?php
/**
* @author Leboc Philippe
* @version 1.0
* @descr Un contrôleur d'exemple :)
*/
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

// initialisation
$title = "Liste des restaurant";
$data = "";

$tousLesRestaurants = DBH::getList('Service',
        array(
            "of_Type" => 1
            )
        );

foreach ($tousLesRestaurants as $resto) {
	$tpl->value('image',$resto->getCompany()->getPicture());
	$tpl->value('image_title',$resto->getCompany()->getName());
	$tpl->value('service_title',$resto->getName());
	$tpl->value('service_description',$resto->getDescription());
	$tpl->value('price',$resto->getPrice());
	$tpl->value('type','/menu');

	$data.=$tpl->build('reservation/for_service');
}

$tpl->value('title', $title);
$tpl->value('boucle', $data);
$page.=$tpl->build('reservation/programmation');