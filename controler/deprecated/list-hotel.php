<?php
/**
* @author Darbon Bertrand
* @version 1.0
* @descr Un contrôleur d'exemple :)
*/
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

// initialisation
$title = "Liste des hotels";
$data = "";

$toutLesHotels = DBH::getList('Service',
        array(
            "of_Type" => 2
            ),
        array(
        	"price" => "asc"
        	)
        );

foreach ($toutLesHotels as $hotel) {
	$tpl->value('image',$hotel->getCompany()->getPicture());
	$tpl->value('image_title',$hotel->getCompany()->getName());
	$tpl->value('service_title',$hotel->getName());
	$tpl->value('service_description',$hotel->getDescription());
	$tpl->value('price',$hotel->getPrice());
	$tpl->value('type','/nuit');

	$data.=$tpl->build('reservation/for_service');
}

$tpl->value('title', $title);
$tpl->value('boucle', $data);
$page.=$tpl->build('reservation/programmation');