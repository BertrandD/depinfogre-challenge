<?php
/**
* @author Leboc Philippe
* @version 1.0
*/
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

// Variables du controller :
$notif = array();	// conteneur des notifications
$pageNotifs = NULL;	// page des notifications
$continue = TRUE;	// false s'il faut afficher une ou plusieurs erreurs
$success = FALSE;	// true si tout est OK

if($user){
	if($user->getAccess_level() != AccessLevel::COMPANY){
		if(!empty($_POST)){
			// Le formulaire à été envoyé ->
			// Extraction des variables de _POST
			extract($_POST);

			// Validation de chacun des champs
			if(validate_string($companyName, 2, 30) && validate_phone($phonePro) && 
				validate_phone($phoneClient) && validate_mail($mailClient) && 
				validate_string($address, 8, 100)){

				// traitement de la description non obligatoire
				$desc = NULL;
				if(!empty($description)){
					if(validate_string($description, 10, 500)){
						$desc = $description;
					}else{
						$notif[] = _("La description doit être composée de 10 à 500 caractères.");
					}
				}else{
					$desc = _('Cette entreprise n\'a pas de description.');
				}

				if(empty($notif)){
					// traitement de l'image
					$picture = NULL;

					if(!empty($_FILES)){
						$picture = validate_file($_FILES, $user->getId());
					}

					// Tous les champs sont valide
					// Création de l'objet compagnie et sauvegarde en base de donnée
					$data = array("id" => DBH::getNextId('Company'),
								"name" => $companyName,
								"website" => $website,
								"description" => $desc,
								"picture" => $picture,
								"responsible" => $user->getId(),
								"privatePhone" => $phonePro,
								"publicPhone" => $phoneClient,
								"publicMail" => $mailClient,
								"address" =>$address);

					$company = DBH::create('Company', $data);
					
					if(!empty($company)){
						DBH::save($company);

						// L'utilisateur devient un professionnel
						$user->setAccess_level(AccessLevel::COMPANY);
						DBH::save($user);

						// On affiche la page permettant d'enregistrer l'activité du professionnel
						// La page suivante quoi
						$success = TRUE;
					}else{
						$notif[] = _("Une erreur interne s'est produite.");
					}
				}
			}else{
				$notif[] = _("Tous les champs n'ont pas été correctement renseignés !");
			}
		}
	}else{
		// User a déjà enregistré sa compagnie
		// Redirection de $user sur la page adéquate
		$continue = TRUE;
	}
}else{
	// Si l'utilisateur n'est pas censé se trouver sur cette page
	// un message spécifique lui est affiché et le contenu est caché.
	$continue = FALSE;
	$notif[] = _("Vous devez être connecté pour accéder à cet espace.");
}

// Build des notifications et de la page.
if($continue){
	if($user->getAccess_level() != AccessLevel::COMPANY){
		if(!empty($notif)){
			foreach ($notif as $msg){
				$tpl->value('messageErreur', $msg);
				$pageNotifs .= $tpl->build('notif/erreur');
			}
			$tpl->value('notifications', $pageNotifs);
		}

		if($success){
			$tpl->value('messageSuccess', _("Félicitation !"));
			$pageNotifs .= $tpl->build('notif/success');
			$page .= $tpl->value('notifications', $pageNotifs);
			$page .= $tpl->build('professionnel/pro-index');
		}else{

			if(!empty($_POST)){
				$tpl->value('name', $companyName);
				$tpl->value('website', $website);
				$tpl->value('phone_pro', $phonePro);
				$tpl->value('phone_cli', $phoneClient);
				$tpl->value('email_cli', $mailClient);
				$tpl->value('description', $description);
				$tpl->value('address', $address);
			}

			$tpl->value("title",_("Activation de l'espace professionnel"));
			$tpl->value('url','pro-register-company');
			$page .= $tpl->build('professionnel/pro-register-company');
		}
	}else{
		header("Location: index.php?op=pro-main");
	}
}else{
	$tpl->value('messageErreur', $notif[0]);
	$tpl->value('url','pro-register-company');
	$notification = $tpl->build('notif/erreur');
	$page .= $tpl->value('notifications', $notification);
	$page .= $tpl->build('access_denied');
}