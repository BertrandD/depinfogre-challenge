<?php
/**
* @author = Jayet Jules
* @version 1.0
* @Descr = Affiche le détail d'un type de service.
*/

// Ne peut être appelé que par le controleur frontal.
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

// Vérification de la présence du paramètre.
if(isset($_GET['type_service_id']))
{

	// Récupération des informations relatives au type de service.
	$type_service=DBH::getUnique('TypeService',array("id"=>$_GET['type_service_id']));

	// Récupération des services associés.
	$services=DBH::getList('Service',array("Of_type"=>$type_service->getId()));

	// Ajout à la page de l'url de la page précedente.
	$tpl->value('return',$_SERVER["HTTP_REFERER"]);

	// Parcours de tous les services.
	$data_services="";
	foreach($services as $value){

		// Id du service.
		$id=$value->getId();

		// Nom du service.
		$name=$value->getName();

		// Ajout à la liste des autres services.
		$tpl->value('service_id',$id);
		$tpl->value('service_name',$name);
		$data_services.=$tpl->build('type_service/service_type_service');

	}
	if(!$data_services) $data_services='<li class="list-group-item"><p>Pas de service pour cette entreprise.</p></li>';
	$tpl->value('service_advices',$data_services);

	// Ajout à la page du nom du type de service.
	$tpl->value('type_service_name',$type_service->getName());

	// Ajout à la page de la description du type de service.
	$tpl->value('type_service_description',$type_service->getDescription());

	// Construction de la page.
	$tpl->value('url','detail_type_service');
	$page.=$tpl->build('type_service/detail_type_service');
}

?>