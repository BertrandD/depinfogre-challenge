<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

if(!empty($user)){


	if(!empty($_GET['date']))
	{
		$tpl->value('dateDeb',date('Y-m-d', strtotime($_GET['date'])));
	}else{
		$journey = $user->getNextJourney();
		if(!empty($journey))
			$tpl->value('dateDeb',date('Y-m-d', strtotime($journey->getStart())));
		else
			$tpl->value('dateDeb', date('Y-m-d'));
	}
	$tpl->value('reservationsWithoutDate',_("Chargement..."));

	$tpl->value('url','recap');
	$tpl->value('lang',$locale);

	$page.=$tpl->build('recap/recap');
}