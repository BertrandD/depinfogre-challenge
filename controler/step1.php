<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}


// Affichage de tous les types d'hébergements à part des autres catégories.
$listAccomodations = DBH::getList('TypeService',array("category"=>1));
// Liste au format HTML de tous les types de services associées à la catégorie 'Hébergement'
$listeHeb = "";
foreach ($listAccomodations as $accomodation) {
	$tpl->value('actId',$accomodation->getId());
	$tpl->value('actName',$accomodation->getName());
	$listeHeb.=$tpl->build('reservation/small/listAccomodation');
}
$tpl->value('listAccomodation',$listeHeb);




// Affichage de tous les types service via un menu déroulant (selon leurs catégories)
$catServices = DBH::getList('TypeService',array("category"=>0, "id"=>array("!=",1)));
// Liste au format HTML de toutes les catégories de services (+ leurs sous-types associés)
$listeCat="";
$catpos=0;
foreach ($catServices as $category) {
	$id = $category->getId();
	$typeServices = DBH::getList('TypeService',array("category"=>$id));
	$tpl->value('category',$category->getName());
	$tpl->value('catpos',$catpos);

	// Liste au format HTML de tous les types de services associées à la catégorie concerné
	$listeType="";
	$pos=0;

	foreach ($typeServices as $typeService) {
		$tpl->value('actId',$typeService->getId());
		$tpl->value('actName',$typeService->getName());

		// Affichage sur 3 colonnes des types de services
		if($pos==0) {
			$listeType.='      	<div class="panel-body">';
		}
		$listeType.=$tpl->build('reservation/small/listeActivitiesUnique');
		if($pos==2) {
			$listeType.='    </div>';
		}
		$pos++;
		$pos%=3;
	}
	if($pos!=0) {
		$listeType.='    </div>';
	}

	$tpl->value('listeType',$listeType);
	$listeCat.=$tpl->build('reservation/small/listeActivities');
	$catpos++;
}

// Construction du 'content' de 'principal.html'
$tpl->value('listActivities',$listeCat);
$tpl->value('url','step1');
$tpl->value('dateStart',date('m/d/Y'));
$tpl->value('dateEnd',date('m/d/Y'));
$page.=$tpl->build('reservation/step1');