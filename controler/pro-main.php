<?php
/**
* @author Leboc Philippe
* @version 1.0
*/
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}



function printSelectTypes()
{
	global $tpl;
	$alltypes = DBH::getList('TypeService', array(), array(), "id, name");
	$options_type_service = NULL;

	if(!empty($alltypes)){
		foreach ($alltypes as $one_of_all){
				$tpl->value('type_service_id', $one_of_all->getId());
				$tpl->value('type_service_name', $one_of_all->getName());
				$tpl->value('isselected', "");
				$options_type_service .= $tpl->build('professionnel/small/option_type_service');
		}
		$tpl->value('options_TS', $options_type_service);
	}
}


// TODO: Gestion des notifications de la page
if(!empty($user) && ($user->getAccess_level() == AccessLevel::COMPANY)){

	// Paramètres d'affichage des pages
	$show_normal_page = FALSE;
	$show_edit_info_page = FALSE;

	// Récupération de la compagnie de $user
	$company = DBH::getUnique('Company', array('responsible' => $user->getId()));

	if(!empty($company)){
		// All check are done, what do you want now ?
		if(!empty($_POST)){
			// Un utilisateur à appuyer sur un envoi de formulaire
			// :: Gestion des boutons ::

			// Pour chaque champs, crée une variable du nom égal à l'attribut name="..."
			extract($_POST);

			if(!empty($button)){
				if($button === 'editinfo'){

					$data_info = NULL;
					$button_info = NULL;

					$tpl->value('companyname', $company->getName());
					$tpl->value('companyweb', $company->getWebsite());
					$tpl->value('companymail', $user->getMail());
					$tpl->value('companyphone', $company->getPrivatePhone());
					$tpl->value('companymailcli', $company->getPublicMail());
					$tpl->value('companyphonecli', $company->getPublicPhone());

					// normal page
					$data_info .= $tpl->build('professionnel/small/show_infos_edit');
					$tpl->value('show_info', $data_info);

					// Le bouton "modifier les informations devient << Valider les modifications >>"
					$button_info .= $tpl->build('professionnel/small/button_infos_validate');
					$tpl->value('button_infos_edit', $button_info);

					// Le bouton de suppression de l'espace pro est désactivé
					$tpl->value('buttonstatus', 'disabled');

					$show_normal_page = TRUE;
					$show_edit_info_page = TRUE;

				}elseif($button === 'validate_info'){
					// Valide les informations et affiche la page d'index de l'espace pro
					if(validate_string($companyname, 2, 30) && validate_phone($companyphone) &&
						validate_phone($companyphonecli) && validate_mail($companymailcli) && 
						validate_string($companyweb, 4, 100)){

						$company->setName($companyname);
						$company->setPrivatePhone($companyphone);
						$company->setWebsite($companyweb);
						$company->setPublicPhone($companyphonecli);
						$company->setPublicMail($companymailcli);

						// enregistre les modifications en base de données
						DBH::save($company);

						// Permet d'afficher à nouveau la page normale et de réactiver les boutons qui ont été désactivé.
						$show_normal_page = TRUE;
					}else{
						// Un des champs n'est pas valide (notification)
						// TODO: NOTIFICATION - Un champ n'est pas correct (OSEF de savoir lequel)
						$show_normal_page = TRUE;
					}
				}elseif($button === 'service_edit'){
					// Recupere le service dont l'id est l'ID passée en paramètre et 
					// le service faisant partie de la compagnie de $user
					$service = DBH::getUnique('Service', array("linked" => $company->getId(), "id" => $service_id));

					if($service){
						// Si le service existe et donc qu'il appartient bien à celui qui veut l'éditer, 
						// on lui affiche la page d'édition du service désiré
						$tpl->value('service_id', $service->getId());
						$tpl->value('name', $service->getName());
						$tpl->value('service_description', $service->getDescription());
						$tpl->value('image', $company->getPicture()); // WHAT ? $service->getPicture() ! WHERE IS BRYAN ??
						$tpl->value('image_title', $service->getName());
						$tpl->value('price', $service->getPrice());
						$tpl->value('type', $service->getType()->getName());

						$alltypes = DBH::getList('TypeService', array(), array(), "id, name");
						$options_type_service = NULL;

						if(!empty($alltypes)){
						foreach ($alltypes as $one_of_all){
								$tpl->value('type_service_id', $one_of_all->getId());
								$tpl->value('type_service_name', $one_of_all->getName());

								if($service->getType()->getId() == $one_of_all->getId()){
									$tpl->value('isselected', "selected");
								}else{
									$tpl->value('isselected', ""); // peut être inutile.
								}

								$options_type_service .= $tpl->build('professionnel/small/option_type_service');
								$tpl->value('options_TS', $options_type_service);
							}
						}
						
						// J'ai pas trouvé mieux pour placer le "selected" dans l'option choisie.
						if($service->getAvailability() == 0)
							$tpl->value('availability', '<option value=\'1\'>Disponible</option><option value=\'0\' selected>Indisponible</option>');
						else
							$tpl->value('availability', '<option value=\'1\' selected>Disponible</option><option value=\'0\'>Indisponible</option>');

						$people = explode(',', $service->getNb_people());
						$tpl->value('people_min', $people[0]);
						$tpl->value('people_max', $people[1]);

						// Création de la page

						$page .= $tpl->build('professionnel/edit_service');
					}else{
						// TODO: Notification - Service doesn't exist
					}
				}elseif($button === 'service_remove'){

					$service = DBH::getUnique('Service', array('linked' => $company->getId(), 'id' => $service_id));

					if($service){
						DBH::removeObject($service);
						// TODO : success message
					}else{
						// TODO: error message
					}

					$show_normal_page = TRUE;

				}elseif($button === 'service_edit_validate'){
					$service = DBH::getUnique('Service', array('linked' => $company->getId(), 'id' => $service_id));

					if($service){
						// il s'agit bien du service de cet utilisateur donc
						// on controle les champs pour les valider et terminer l'édition
						if(validate_string($name, 3, 50) && validate_string($description, 10, 3000) &&
							validate_numeric($type, 0, 9999) && validate_numeric($price, 0, 999) &&
							validate_numeric($people_min, 0, 998) && validate_numeric($people_max, 1, 999) && validate_numeric($availability, 0, 1)){

							$service->setName($name);
							$service->setDescription($description);
							$service->setAvailability($availability);
							$service->setPrice($price);
							$service->setOf_Type($type);
							//$service->setPicture(); // special
							$people = $people_min . ',' . $people_max;
							$service->setNb_people($people);

							DBH::save($service);

							// Retour à la page normale
							$show_normal_page = TRUE;
						}else{
							// TODO: Notification - champ invalide
						}
					}else{
						// TODO: Notification - Service doesn't exist
					}
				}elseif($button === 'service_add'){
					printSelectTypes();
					$page .= $tpl->build('professionnel/add_service');
				}elseif($button === 'service_add_validate'){
					// check all field
					if(validate_string($name, 3, 50) && validate_string($description, 10, 3000) &&
							validate_numeric($price, 0, 9999) && validate_numeric($type, 1, 999999) &&
							validate_numeric($people_min, 0, 998) && validate_numeric($people_max, 1, 999) && validate_numeric($availability, 0, 1)){

						$service = DBH::create('Service', array('id' => DBH::getNextId('Service'),
																'name' => $name,
																'description' => $description,
																'availability' => $availability,
																'price' => $price,
																'linked' => $company->getId(),
																'nb_people' => $people_min . ',' . $people_max,
																'Of_Type' => $type));
						DBH::save($service);

						$show_normal_page = TRUE;
					}else{

						printSelectTypes();
						$tpl->value('name', $name);
						$tpl->value('description', $description);
						//$tpl->value('availability', $availability);
						$tpl->value('price', $price);
						$tpl->value('people_min', $people_min);
						$tpl->value('people_max', $people_max);

						$page .= $tpl->build('professionnel/add_service');
					}
				}else{
					// Normal page
					$show_normal_page = TRUE;
				}
			}else{
				// $button doesn't exist
				$show_normal_page = TRUE;
			}
		}else{
			// Show me a normal page...
			// Info non editables
			$show_normal_page = TRUE;
		}

		// Pour éviter la duplication de code.
		if($show_normal_page){
			// Affichage des informations s'ils ne sont pas en cours d'édition
			if(!$show_edit_info_page){
				$tpl->value('companyname', $company->getName());
				$tpl->value('companyweb', $company->getWebsite());
				$tpl->value('companymail', $user->getMail());
				$tpl->value('companyphone', $company->getPrivatePhone());
				$tpl->value('companymailcli', $company->getPublicMail());
				$tpl->value('companyphonecli', $company->getPublicPhone());

				$data_info = $tpl->build('professionnel/small/show_infos');
				$tpl->value('show_info', $data_info);

				$button_info = $tpl->build('professionnel/small/button_infos_edit');
				$tpl->value('button_infos_edit', $button_info);

				// Le bouton de suppression de l'espace pro est activé
				$tpl->value('buttonstatus', '');
			}

			// Affichage des services
			$services = DBH::getList('Service', array('linked' => $company->getId()));
			$data = NULL;

			foreach ($services as $service) {
				$tpl->value('service_id', $service->getId());
				$tpl->value('service_title', $service->getName());
				$tpl->value('service_description', $service->getDescription());
				$tpl->value('image', $company->getPicture()); // WHAT ? $service->getPicture() ! WHERE IS BRYAN ??
				$tpl->value('image_title', $service->getName());
				$tpl->value('price', $service->getPrice());
				//$tpl->value('type', $service->getType()->getSmallText()); // WHERE IS BRYAN ??
				$data .= $tpl->build('professionnel/small/show_services');
				$tpl->value('for_services', $data);
			}

			$form_infos = $tpl->build('professionnel/small/form_infos');
			$tpl->value('form_infos', $form_infos);

			$page .= $tpl->build('professionnel/pro-index');
		}
	}else{
		$page .= $tpl->build('access_denied');
	}
}else{
	$page .= $tpl->build('access_denied');
}