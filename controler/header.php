<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

$user = null;
$bagElemCount = 0;

if(isset($_SESSION['id']))
{
	$identifier=array(
		'id' => $_SESSION['id']
		);
	$user=DBH::getUnique('User',$identifier);
}

if(isset($_POST['login']) && isset($_POST['pass']))
{
	// Traitement des données
	$login = htmlspecialchars($_POST['login']);
	$pass = htmlspecialchars($_POST['pass']);

	// Le mot de passe arrive ici en étant cripté par javascript en sha1
	// Cette méhode de cryptage est un peu faible et peu adapté à des mots de passe
	// car plutôt pensée pour des fichiers, une utilisation de crypt() serait plus adaptée
	// mais n'étant pas comprise par mysql, il faudrait tester les mots de passe de la BDD
	// un par un avec cette condition :  hash_equals($hashed_password, crypt($user_input, $hashed_password))
	// où $hashed_password est le vrai mot de passe et $user_input le mot de passe entré par l'utilisateur
	// dans notre le cas, l'utilisation de sha1 est bien suffisante ! D'autant que nous
	// utilisons du HTTPS... 
	$identifier=array(
		"mail" => $login,
		"password" => $pass,
		);
	$user = DBH::getUnique('User',$identifier);

	if(empty($user))
	{
		$tpl->value('erreur', _("erreur :)"));
	}else{
		$_SESSION['id'] = $user->getId();
	}
}

if(isset($_POST['deconnexion']))
{
	session_destroy();
	$user=null;
}

if(empty($user))
{
	$tpl->value("user",$tpl->build('authentification/formulaire_connexion'));
}else{
	$bagElemCount = $user->getNbBagElements();
	
	$tpl->value("login", $user->getFirstName()." ".$user->getName());
	$tpl->value("bagElemCount", $bagElemCount);
	$tpl->value("mail", $user->getMail());
	$tpl->value("user",$tpl->build('authentification/infos_user'));
}