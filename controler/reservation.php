<?php
/**
* @author Leboc Philippe
* @version 2.0
* @descr contrôleur générique pour toute la procédure de réservation (mettre au panier).
*/
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

if(!empty($user)){
	if(!empty($_POST['service']) && !empty($_POST['journey'])){
		// Suppression d'un objet du panier
		// Traitement des informations reçues
		$serviceId = htmlspecialchars($_POST['service']);
		$journeyId = htmlspecialchars($_POST['journey']);

		if(is_numeric($serviceId) && is_numeric($journeyId)){
			// Récupération de la réservation à supprimer
			$resaToBeDeleted = DBH::getUnique('Reservation',
				array("UserId" => $user->getId(), "State" => '0', "Journey" => $journeyId, "Concern" => $serviceId));
			if(!empty($resaToBeDeleted)){
				// Destruction de la réservation
				DBH::removeObject($resaToBeDeleted);

				// Affichage du panier (une chose bien sale ci-dessous :p)
				header("Location: index.php?op=reservation");
			}else{
				$tpl->value('messageErreur', "Vous essayez de supprimer une réservation qui n'existe pas !");
				$page.=$tpl->build('notif/erreur');
			}
		}else{
			$tpl->value('messageErreur', "Vous avez envoyé des données incorrectes !");
			$page.=$tpl->build('notif/erreur');
		}
	}else{
		// Affichage normal du panier
		// initialisations
		$title = "Récapitulatif";
		$data = NULL;
		$total = 0;

		$panier = $user->getBag();
		$total=0;
		if(!empty($panier)){
			foreach ($panier as $element){
				$tpl->value('service_title',$element->getService()->getName());
				$tpl->value('price',$element->getService()->getPrice());
				$tpl->value('service_id',$element->getConcern());
				$tpl->value('journey_id',$element->getJourney());
				$total += $element->getService()->getPrice();
				$data.=$tpl->build('panier/for_panier');
			}
		}else{
			$title = "Panier vide";
		}

		$tpl->value("title", $title);
		$tpl->value('boucle', $data);
		$tpl->value('total',$total);
		$page.=$tpl->build('panier/panier');
	}
}else{
	$tpl->value('messageErreur', "Vous devez être connecté pour accéder à cette page !");
	$page.=$tpl->build('notif/erreur');
}