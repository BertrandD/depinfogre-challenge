<?php
/**
* @author Darbon Bertrand & Delolme Alexandre
* @version 2.0
* @descr C'est lui qui aura pour rôle de gérer l'outil de recherche
* 		C'est aussi ici qu'on viendra après step1, bref, dès qu'il faut 
*		faire une recherche ou une liste de qqch, c'est ici.
*/
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}


$tpl->value('notifJourney','');
// Retour du formulaire du step1.php
if(!empty($_GET['dateStart']) && !empty($_GET['dateEnd']))
{
	extract($_GET);
	$journey = DBH::getUnique('Journey', array(
		'name' => _('Les vacances de '.$user->getName()),
		'start' => date('Y-m-d H:i:s', strtotime($dateStart)),
		'end' => date('Y-m-d H:i:s', strtotime($dateEnd)),
		'place' => $PostalCodeStart,
		'nb_people' => $NbAdults+$NbEnfants,
		'user_id' => $user->getId()
		));
	if(empty($journey))
	{
		$journey = DBH::create('Journey', array(
			'name' => _('Les vacances de '.$user->getName()),
			'start' => date('Y-m-d H:i:s', strtotime($dateStart)),
			'end' => date('Y-m-d H:i:s', strtotime($dateEnd)),
			'place' => $PostalCodeStart,
			'nb_people' => $NbAdults+$NbEnfants,
			'user_id' => $user->getId()
			));
		if(!empty($journey))
		{
			DBH::save($journey);
			$tpl->value('messageSuccess',_('Félicitation, votre projet a été créé, vous pouvez désormais commencer à parcourir les offres proposées et réserver celles qui vous intéressent.'));
			$tpl->value('notifJourney',$tpl->build('notif/success'));
		}else{
			$tpl->value('messageErreur',_("Une erreur s'est produite..."));
			$tpl->value('notifJourney',$tpl->build('notif/erreur'));
		}
	}else{
			$tpl->value('messageErreur',_("Vous avez déjà un projet avec exactement les mêmes dates..."));
			$tpl->value('notifJourney',$tpl->build('notif/erreur'));
	}
}

/* MODELE DE RECHERCHE
*	Radical de la recherche
*		?op=search
*	Suffixe "formulaire" => Du texte, plusieurs mots possible par request[x], une expression peut être mise entre guillemets, pour exclure un terme, mettre un '-' devant
*		&request[]=Chambre&request[]=-Chambre&request[]="Chambre Single"&request[]=Chambre- Chambre "Chambre Single"
*	Suffixe "type" => Identifiant(s) de la/des catégorie(s) ou sous-catégorie(s) à afficher (à ne pas afficher si '-' devant)
*		&type[]=1&type[]=200type[]=-2
*	Suffixe "dispo" => 1 pour les services disponibles uniquement, 2 pour l'inverse; par défaut pas de tri
*		&dispo=1 || &dispo=1 || rien
*	Suffixe "owner" => Identifiants du/des entreprises à afficher, ou à ne pas afficher si '-' devant
*		&owner[]=1&owner[]=-2
*	Suffixe "advice" => Intervalle min-max des notes moyennes des offres à afficher
*		&advice[]=1&advice[]=5 || &advice[]=5&advice[]=3
*	Suffixe "price" => Pourcentages min-max des tarifs des offres à afficher (selon prix max et min de chaque sous-catégorie)
*		&price[]=3&price[]=50 || &price[]=100&price[]=25
*	Suffixe "nb_people" => Intervalle min-max des personnes pouvant participer aux activités
*		&nb_people[]=3&nb_people[]=5 || &nb_people[]=8&nb_people[]=0
*	Suffixes "order" et "sens" => Précisent l'ordre à appliquer sur les offres résultantes et le sens (croissant/décroissant)
*		order = 0 => Tri par pertinence
*		order = 1 => Tri par prix
*		order = 2 => Tri par nombre de personnes
*		order = 3 => Tri par notation
*		order = 4 => Tri par disponibilité (sens=1 => Ceux disponible en premier/ 0 => ceux non dispo en premier)
*		order = 5 => Tri selon le nom de la sous-catégorie
*		order = 6 => Tri selon les noms des entreprises proposant les services
*		order = 7 => Tri selon le nom du service
*		sens = 0 => Tri par ordre croissant.
*		sens = 1 => Tri par ordre décroissant
*	Suffixes "nb_offer" et "currentPage" => Précisent le nombre d'offres affichables par page et la page courante
		1 <= nb_offer <= 50
		Si 3 page en tout, currentPage= 1 || 4 || -2 afficheront la premère, currentPage= 0 || 3 afficheront la dernière...
*
* REMARQUE :
*   La catégorie (et donc l'id) N°99 est réservé pour le 'hasard'
*
* EXEMPLES :
* 	index.php?op=search 	Requête de base affichant tous les services proposés et a termes également les entreprises et types de services...
* 	index.php?op=search&type[]=2	Affiche tous les services associés au type d'id=2 (et ses sous-types si c'est une catégorie) -> ici tous les services de restauration
*	index.php?op=search&type[]=200	Affiche uniquement les restaurants
* 	index.php?op=search&type[]=-1 	Affichera tous les services sauf ceux d'hébergement
*	index.php?op=search&type[]=101&type[]=200&type[]=-1 	Affichera uniquement les restaurants puisque ntype=1 cache les hébergement et donc les hotels
*
*	index.php?op=search&nb_people[]=1&nb_people[]=10 	Affichera tous les services associé à au moins 1 personne
*	index.php?op=search&nb_people[]=5&nb_people[]=0	Affichera tous les services associé à moins de 5 personnes
*
*	index.php?op=search&price[]=50price[]=100	Affichera les services dont le prix est supérieur ou égal au prix du service médian
*	index.php?op=search&price[]=100&price[]=0	Affichera tous les services

*	index.php?op=search$advice[]=3$advice[]=5 	Affichera les services dont la notation moyenne est supérieure à 3/5
*	index.php?op=search$advice=3$advice[]=0 	Affichera les services dont la notation moyenne est inférieure à 3/5
*
*	index.php?op=search&dispo=0 	Affiche les services disponibles
*	index.php?op=search&dispo=1 	Affiche les services non disponibles
*
*	index.php?op=search&owner[]=1 	Affiche tous les services associés à l'entreprise d'id=1
*	index.php?op=search&owner[]=-1&owner[]=-2 	Affiche tous les services sauf ceux associés aux entreprises d'id=1 ou 2
*
* Voila tous les attributs pour le moment, je prévois des améliorations, je vous incite à tester de vous-même pour repérer les problèmes :)
* AMELIORATIONS PREVUES :
*	- Modifier dispo, peut être rajouter un attribut 'season' aux services pour connaître les saisons durant lesquelles ils sont proposés
*	- Rajouter des packs de services ?
*	- Faire l'interface du moteur de recherche
*/

// initialisation
$data = "";
$collapseId = 0;
extract($_GET);
$notifs='';

// REDIRECTION TEMPORAIRE de step1.php vers search.php
if(isset($hebergement)) {
	if($hebergement!=0) {
		$type[] = $hebergement;
	}
}
$nb_people[]=0;
if(isset($NbAdults)) {
	$nb_people[0] += $NbAdults;
}
if(isset($NbEnfants)) {
	$nb_people[0] += $NbEnfants;
}

// Paramètres des requêtes préparées
$paramSer = array();
$pSer = "";
$table = array();

// TYPE : Précise le type des activités recherchées
$inType = array();
$outType = array();
if(!empty($type) && is_array($type)) {
	foreach($type as $elem) {
		if($elem < 0) {
			$outType[] = -$elem;
		} else {
			$inType[] = $elem;
		}
	}
	if(!empty($inType) || !empty($outType)) {
		$p = "";
		$n = "";
		if(!empty($inType)) {
			$p = ") OR category IN (";
			$separateur = "";
			foreach($inType as $key => $elem) {
				$p = " :i".$key.$separateur.$p.$separateur." :c".$key;
				$param[":i".$key] = $elem;
				$param[":c".$key] = $elem;
				if(!$separateur) {
					$separateur=",";
				}
			}
			$p = " AND (id IN (".$p."))";
		}
		if(!empty($outType)) {
			$n = ") AND category NOT IN (";
			$separateur = "";
			foreach($outType as $key => $elem) {
				$n = " :ni".$key.$separateur.$n.$separateur." :nc".$key;
				$param[":ni".$key] = $elem;
				$param[":nc".$key] = $elem;
				if(!$separateur) {
					$separateur=",";
				}
			}
			$n = " AND id NOT IN (".$n.")";
		}
		$req = Database::getInstance()->prepare('SELECT id FROM Type_Service WHERE category != 0'.$p.$n);
		try {
			$req->execute($param);
		} catch (Exception $e) {
			printR($e->getMessage().'<br/>SELECT id FROM Type_Service'.$p.$n);
		}
		$donnees = $req->fetchall(PDO::FETCH_COLUMN,0);
		if(!empty($donnees)) {
			$pSer = " Of_Type IN (";
			$separateur = "";
			foreach($donnees as $key => $elem) {
				$pSer .= $separateur." :t".$key;
				$paramSer[":t".$key] = $elem;
				if(!$separateur) {$separateur=',';}
			}
		$pSer .= ")";
		} else {
			$tpl->value('messageErreur',"Erreur, aucune catégorie ne correspond au(x) paramètre(s) 'type' !");
			$notifs.=$tpl->build('notif/erreur');
		}
	}
}
// DISPO
if(isset($dispo)) {
	switch($dispo) {
		case 1 :
			if($pSer) {$pSer.=" AND";}
			$pSer .= " availability = 1";
		break;
		case 2 :
			if($pSer) {$pSer.=" AND";}
			$pSer .= " availability = 0";
		break;
	}
}
// ADVICE
// Enlève tous les services qui ont une note non comprise dans l'intervalle passé en paramètre
if(isset($advice) && is_array($advice) && sizeof($advice)==2) {
	sort($advice);
	if($pSer) {$pSer.=" AND";}
	$pSer .= " id NOT IN (SELECT Service FROM (SELECT avg(mark) as mark, Service FROM Advice GROUP BY Service) A WHERE mark < :a1 OR mark > :a2)";
	$paramSer[":a1"] = $advice[0];
	$paramSer[":a2"] = $advice[1];
}

// Récupération des offres correspondantes aux paramètres analysés
if($pSer) {$pSer = " WHERE".$pSer;}
$req = Database::getInstance()->prepare('SELECT * FROM Service'.$pSer);
try {
	$req->execute($paramSer);
} catch (Exception $e) {
	printR($e->getMessage().'<br/>SELECT * FROM Service'.$pSer);
}
while($donnees = $req->fetch()){
	$table[$donnees["id"]] = new Service($donnees);
}
if(!$table) {
	$tpl->value('messageErreur',"Erreur, aucun résultat trouvé pour les paramètres spécifiés !");
	$notifs.=$tpl->build('notif/erreur');
} else {
	// OWNER
	$def = false;
	$ownerUtility = array();
	if(isset($owner) && is_array($owner)) {
		foreach($owner as $elem) {
			if($elem<0) {
				$ownerUtility[-$elem] = 3;
			} else {
				$ownerUtility[$elem] = 4;
				$def = true;
			}
		}
	}
	foreach($table as $key => $elem) {
		$ow = $elem->getLinked();
		if(array_key_exists($ow,$ownerUtility)) {
			if($ownerUtility[$ow]>2) {
				$ownerUtility[$ow]-=2;
			}
			if($ownerUtility[$ow]==1 || $ownerUtility[$ow]==0 && $def) {
				unset($table[$key]);
			}
		} else {
			$ownerUtility[$ow] = 0;
			if($def) {
				unset($table[$key]);
			}
		}
	}

	if(!$table) {
		$tpl->value('messageErreur',"Erreur, aucun résultat trouvé pour les entreprises spécifiées !");
		$notifs.=$tpl->build('notif/erreur');
	} else {
		// PRICE
		$maxPrice="";
		$minPrice="";
		foreach($table as $elem) {
			if(!$maxPrice) {$maxPrice=$elem->getprice();}
			if(!$minPrice) {$minPrice=$elem->getprice();}
			if($maxPrice<$elem->getprice()) {
				$maxPrice=$elem->getprice();
			}
			if($minPrice>$elem->getprice()) {
				$minPrice=$elem->getprice();
			}
		}
		if(isset($price) && is_array($price) && sizeof($price)==2) {
			sort($price);
			foreach($table as $key => $elem) {
				if($elem->getPrice() < $price[0] || $elem->getPrice() > $price[1]) {
					unset($table[$key]);
				}
			}
		}
		if(!$table) {
			$tpl->value('messageErreur',"Erreur, aucun résultat trouvé pour l'intervalle de prix spécifié !");
			$notifs.=$tpl->build('notif/erreur');
		} else {
			// NB_PEOPLE
			if(isset($nb_people) && is_array($nb_people)) {
				if(sizeof($nb_people)==2) {
					sort($nb_people);
					foreach($table as $key => $elem) {
						$nb = explode(",",$elem->getNb_people());
						if($nb[0]<$nb_people[0] || $nb[1]>$nb_people[1]) {
							unset($table[$key]);
						}
					}
				} else if (sizeof($nb_people)==1) {
					foreach($table as $key => $elem) {
						$nb = explode(",",$elem->getNb_people());
						if($nb[1]<$nb_people[0]) {
							unset($table[$key]);
						}
					}
				}
			}
			if(!$table) {
			$tpl->value('messageErreur',"Erreur, aucun résultat trouvé pour l'intervalle du nombre de personnes spécifié !");
			$notifs.=$tpl->build('notif/erreur');
			} else {
				if(!isset($order)) {
					$order = 0;
				}
				if(!isset($sens)) {
					$sens = 0;
				}
				// NB_OFFER (par page)
				if(!isset($nb_offer) || $nb_offer<=0 || $nb_offer > 50) {
					$nb_offer=10;
				}
				// PAGE (courante)
				$nb_page = (integer) (sizeof($table)/$nb_offer);
				if(sizeof($table)%$nb_offer!=0) {
					$nb_page++;
				}
				if(isset($currentPage)) {
					if($nb_page==1) {
						$currentPage=1;
					} else if ($currentPage<1 || $currentPage>$nb_page) {
						if($currentPage<1) {
							$currentPage = $nb_page+$currentPage%$nb_page;

						} else {
							$currentPage = $currentPage%$nb_page;
							if(!$currentPage) {
								$currentPage = $nb_page;
							}
						}
					}
				} else {
					$currentPage = 1;
				}
				$iteration = 0;// Nombre d'offres déjà traités
				// REQUEST (Formulaire/filtres textuel sur le nom des activités, leur description ou leurs mots-clés)
				$vide = true;
				if(!empty($request) && is_array($request)) {
					$i=0;
					while($i<sizeof($request) && $vide) {
						if(!empty($request[$i])) {
							$vide=false;
						}
						$i++;
					}
				}
				if(!$vide) {
					$words = array();
					foreach($request as $elem) {
					$elem = trim($elem);
						$i = 0;
						while($i < strlen($elem)) {
							if($elem[$i]=="-") {
								$not = true;
								$i++;
								while($i < strlen($elem) && $elem[$i]==" ") {
									$i++;
								}
							} else {
								$not = false;
							}
							if($elem[$i]=='"') {
								$sep = '"';
								$i++;
							} else if($elem[$i]=="'") {
								$sep = "'";
								$i++;
							} else {
								$sep = ' ';
							}
							$w = "";
							while($i<strlen($elem) && $elem[$i]!=$sep) {
								$w .=$elem[$i];
								$i++;
							}
							if($w) {
								if($not) {
									$words[$w] = -2;
								} else {
									$words[$w] = 1;
								}
							}
							$i++;
							while($i < strlen($elem) && $elem[$i]==" ") {
								$i++;
							}
						}
					}
					$offer = array();
					foreach($table as $elem) {
						$offer[$elem->getId()] = array();
						foreach($words as $key => $val) {
							$perti = 0;
							$perti += substr_count(strtolower($elem->getName()),strtolower($key))*$val*4;
							$perti += substr_count(strtolower($elem->getName()),strtolower($key))*$val;
							// Nombre de mots-clés *2
							$offer[$elem->getId()][$key] = $perti;
						}
					}
					$offerSum = array();
					$wordsUtility = array_keys($words);
					foreach($offer as $key => $elem) {
						if(!empty($elem)) {
							$offerSum[$key]=array_sum($elem);
						}
						foreach($wordsUtility as $pos => $val) {
							if(array_key_exists($val,$elem) && $elem[$val]!=0) {
								unset($wordsUtility[$pos]);
							}
						}
					}
					arsort($offerSum);
					$offerNeutral = array();
					foreach($offerSum as $key => $elem) {
						if($elem <=0) {
							if($elem ==0) {
								$offerNeutral[$key]=$key;
							}
							unset($offerSum[$key]);
						}
					}
					if(empty($offerSum) && empty($offerNeutral)) {
						$tpl->value('messageInfo',"Aucune offre ne correspond à vos filtres.");
						$notifs.=$tpl->build('notif/info');
					} else if (empty($offerSum)) {
						$tpl->value('messageInfo',"Seules des offres 'neutres' vis à vis des filtres ont été trouvé.");
						$notifs.=$tpl->build('notif/info');
					}
					if(!empty($offerSum)) {
						printOffer(array_keys($offerSum),$order,$sens);
					}
					if(!empty($offerNeutral) && $iteration < $currentPage*$nb_offer) {
						$data .='<div class="panel-heading">Offres neutres</div>';
						printOffer($offerNeutral,$order,$sens);
					}
				} else {
					printOffer(array_keys($table),$order,$sens);
				}
			}
		}
	}
}

function printOffer($liste=array(),$order=0, $sens=0) {
	global $data;
	global $collapseId;
	global $table;
	global $tpl;
	global $currentPage;
	global $nb_offer;
	global $iteration;
	if($order) {
		$p = "";
		$select = "SELECT S.id";
		$from = " FROM Service S";
		$where = " WHERE";
		if(!$sens) {
			$p = " asc";
		} else {
			$p = " desc";
		}
		switch($order) {
			case 1 :
				$p = "ORDER BY S.price".$p;
			break;
			case 2 :
				$p = "ORDER BY S.nb_people".$p;
			break;
			case 3 :
				$req = Database::getInstance()->prepare("SELECT Service FROM Advice ORDER BY mark".$p);
				try {
					$req->execute();
				} catch (Exception $e) {
					printR($e->getMessage().'<br/>SELECT Service FROM Advice ORDER BY mark'.$p);
				}
				$donnees = $req->fetchall(PDO::FETCH_COLUMN,0);
				$p = "ORDER BY FIELD (id,".implode(",",$donnees).",id)";
			break;
			case 4 :
				$p = "ORDER BY S.availability".$p;
			break;
			case 5 :
				$from .= " , Type_Service T";
				$where .= " T.id=S.Of_type AND";
				$p = "ORDER BY T.name".$p;
			break;
			case 6 :
				$from .= " , Company C";
				$where .= " C.id=S.Linked AND";
				$p = "ORDER BY C.name".$p;
			break;
			case 7 :
				$p = "ORDER BY S.name".$p;
			break;
		}
		$param = array();
		$separateur = "";
		$where .= " S.id IN (";
		foreach($liste as $key => $elem) {
			$where .= $separateur.' :i'.$key;
			$param[":i".$key]=$elem;
			if(!$separateur) {$separateur=",";}
		}
		$where .= ")";
		$req = Database::getInstance()->prepare($select.$from.$where.$p);
			try {
				$req->execute($param);
			} catch (Exception $e) {
				printR($e->getMessage().'<br/>'.$select.$from.$where.$p);
			}
		$liste = $req->fetchall(PDO::FETCH_COLUMN,0);
	} else {
		if(isset($sens) && $sens) {
			asort($liste);
		}
	}
	foreach($liste as $elem) {
		if($iteration >=($currentPage-1)*$nb_offer && $iteration < $currentPage*$nb_offer) {
			$serv = $table[$elem];
			$tpl->value('image',$serv->getCompany()->getPicture());
			$tpl->value('image_title',$serv->getCompany()->getName());
			$tpl->value('service_title',$serv->getName());

			if(strlen($serv->getDescription())>50){
				$short_description = substr($serv->getDescription(),0,50);
				$service_description = substr($serv->getDescription(),50);
			}else{
				$short_description = $serv->getDescription();
				$button = "";
				$service_description = "";
			}
			$tpl->value('short_description',$short_description);
			$tpl->value('collapse_id',$collapseId);
			$tpl->value('service_description',$service_description);
			$tpl->value('service_id',$serv->getId());
			$tpl->value('price',$serv->getPrice());
			$tpl->value('type', "/nuit"); // Faudra penser à ajouter un champ dans typeservice pour ça
			$tpl->value('link',$_SERVER['QUERY_STRING']);

			$data.=$tpl->build('reservation/for_service');
			$collapseId++;
		}
		$iteration++;
	}
}

// Construction de la recherche avancée
$advanced_search = "";
// Partie Filtre
$list_filtre = "";
$filtre = "";
if(isset($words)) {
	foreach($words as $key => $value) {
		if(!in_array($key,$wordsUtility)) {
			$tpl->value('word_filtre',$key);
			if($value<0) {
				$tpl->value('sign_filtre',"minus");
				$tpl->value('type_filtre',"danger");
				$tpl->value('value',"-'".$key."'");
			} else {
				$tpl->value('sign_filtre',"plus");
				$tpl->value('type_filtre',"success");
				$tpl->value('value',"'".$key."'");
			}
			$tpl->value('id','P'.$key);
			$list_filtre.=$tpl->build('search/filtre/element');
		}
	}
	foreach($wordsUtility as $key=>$elem) {
		$tpl->value('type_filtre',"default");
		$tpl->value('word_filtre',$elem);
		$tpl->value('id','D'.$key);
		if($words[$elem]<0) {
			$tpl->value('sign_filtre',"minus");
			$tpl->value('value',"-'".$elem."'");
		} else {
			$tpl->value('sign_filtre',"plus");
			$tpl->value('value',"'".$elem."'");
		}
		$list_filtre.=$tpl->build('search/filtre/element');
	}
}
if($list_filtre) {
	$tpl->value('list_filtre',$list_filtre);
	$filtre .= $tpl->build('search/filtre/filtre');
}

//Partie Catégories
$donnees = array();
$cat = array();
$arbo = array();
$otherCat = array();
$req = Database::getInstance()->prepare('SELECT T.id, T.name FROM Type_Service T');
try {
	$req->execute();
} catch (Exception $e) {
	printR($e->getMessage().'<br/>SELECT T.id, T.name FROM Type_Service T');
}
while($donnees = $req->fetch()) {
	$cat[$donnees[0]] = array("name"=>$donnees[1],"count"=>0);
	$arbo[] = 'a'.$donnees[0];
}
sort($arbo);
foreach($arbo as $key =>$elem) {
	$arbo[$key] = substr($elem,1);
}
foreach($cat as $key => $elem) {
	$cat[$key]["present"] = 0;
	if(in_array($key,$inType)) {
		$cat[$key]["present"] = 2;
	}
	if(in_array($key,$outType)) {
		$cat[$key]["present"] = 1;
	}
}
$i = 0;
$nb_cat = 0;
while($i<sizeof($arbo)) {
	$i++;
	if($arbo[$i-1]<100) {
		if($cat[$arbo[$i-1]]["present"]==2) {
			while($i<sizeof($arbo) && $arbo[$i]>=100) {
				if($cat[$arbo[$i]]["present"]!=1) {
					$cat[$arbo[$i]]["present"]=2;
				}
				$i++;
			}
		} else if ($cat[$arbo[$i-1]]["present"]==1) {
			while($i<sizeof($arbo) && $arbo[$i]>=100) {
				$cat[$arbo[$i]]["present"]=1;
				$i++;
			}
		} else {
			$vide = true;
			while($i<sizeof($arbo) && $arbo[$i]>=100) {
				if($cat[$arbo[$i]]["present"]!=0) {
					$vide=false;
				}
				$i++;
			}
			if($vide) {
				$otherCat[] = (int)($arbo[($i-1)]/100);
			}
		}
	}
	$nb_cat++;
}
// Comptage du nombre d'offre trouvé par catégorie
foreach($table as $elem) {
	$cat[$elem->getOf_Type()]["count"]++;
}
// Affichage
$autres_categorie = "";
$element_autres_categorie = "";
$sous_categorie = "";
$categorie = "";
$catChosen = true;
if(sizeof($otherCat)==$nb_cat) {
	$catChosen = false;
}
$i = 0;
while($i<sizeof($arbo)) {
	$other = false;
	if(in_array($arbo[$i], $otherCat) && $catChosen) {
		$other = true;
	}
	$tpl->value('name_cat',$cat[$arbo[$i]]["name"]);
	$tpl->value('cat',$arbo[$i]);
	$idCat = $arbo[$i];
	$i++;
	$sous_categorie = "";
	while($i<sizeof($arbo) && $arbo[$i]>=100) {
		$tpl->value('name_sous',$cat[$arbo[$i]]["name"]);
		if($cat[$arbo[$i]]["present"]==0) {
			$tpl->value('name_type',"default");
			$tpl->value('glyphRight',"minus");
			$tpl->value('glyphLeft',"plus");
			$tpl->value('disabled', 'disabled');
			$tpl->value('sign', '');
		} else if ($cat[$arbo[$i]]["present"]==1) {
			$tpl->value('name_type',"danger");
			$tpl->value('glyphRight',"remove");
			$tpl->value('glyphLeft',"plus");
			$tpl->value('disabled', '');
			$tpl->value('sign', '-');
		} else {
			$tpl->value('name_type',"success");
			$tpl->value('glyphRight',"remove");
			$tpl->value('glyphLeft',"minus");
			$tpl->value('disabled', '');
			$tpl->value('sign', '');
		}
		if($cat[$arbo[$i]]["count"]!=0) {
			$tpl->value('badge_sous',$cat[$arbo[$i]]["count"]);
		} else {
			$tpl->value('badge_sous',"");
		}
		$tpl->value('id', $arbo[$i]);
		$sous_categorie.=$tpl->build('search/categories/sub');
		$i++;
	}
	$tpl->value('sous_categorie',$sous_categorie);
	$tpl->value('chosenCat',1);
	if($catChosen && !$other) {
		$tpl->value('state','true');
		$tpl->value('in','in');
		//$tpl->value('removeMain','<span id="'.$idCat.'Right" class="glyphicon glyphicon-remove-sign pull-right removeMain" value="'.$idCat.'" aria-hidden="true"></span>');
		$categorie.=$tpl->build('search/categories/main');
	} else {
		$tpl->value('state','false');
		$tpl->value('in','');
		if(!$catChosen) {
			//$tpl->value('removeMain','<span id="'.$idCat.'Right" class="glyphicon glyphicon-remove-sign pull-right removeMain" value="'.$idCat.'" aria-hidden="true"></span>');
			$categorie.=$tpl->build('search/categories/main');
		} else {
			//$tpl->value('removeMain','');
			$tpl->value('chosenCat',0);
			$element_autres_categorie.=$tpl->build('search/categories/main');
		}
	}
	
}
if($element_autres_categorie) {
	$tpl->value('element_autres_categorie',$element_autres_categorie);
	$autres_categorie.=$tpl->build('search/categories/others');
}
$tpl->value('autres_categorie',$autres_categorie);
$tpl->value('categorie',$categorie);
$categorie=$tpl->build('search/categories/root');

// Partie Dispo
$optionA_dispo="selected";
$optionD_dispo="";
$optionI_dispo="";
$disponibility="";
if(isset($dispo)) {
	if($dispo==1) {
		$optionA_dispo="";
		$optionD_dispo="selected";
	} else if ($dispo==2) {
		$optionA_dispo="";
		$optionI_dispo="selected";
	}
}
$tpl->value('optionA_dispo',$optionA_dispo);
$tpl->value('optionD_dispo',$optionD_dispo);
$tpl->value('optionI_dispo',$optionI_dispo);
$disponibility .= $tpl->build('search/disponibility');

// Partie Mark
$stars = "";
if(!isset($advice)) {
	$advice = array(0,5);
}
for($k=0; $k<5; $k++) {
	if($k<$advice[0] || $k>$advice[1]) {
		$tpl->value('state_star',"-empty");
	} else {
		$tpl->value('state_star',"");
	}
	$tpl->value('position',$k+1);
	$stars .= $tpl->build('search/advice/star');
}
$tpl->value('star_advice',$stars);
$tpl->value('minAdvice',$advice[0]);
$tpl->value('maxAdvice',$advice[1]);

$stars = $tpl->build('search/advice/advice');

// Partie Price
$priceInterval="";
$maxChosen="";
$minChosen="";
if(!isset($minPrice)) {
	$minPrice="0";
}
if(!isset($maxPrice)) {
	$maxPrice="1000";
}
if(isset($price) && is_array($price) && sizeof($price)==2) {
	sort($price);
	if($price[0]<$minPrice) {
		$minChosen=$minPrice;
	} else {
		$minChosen= $price[0];
	}
	if($price[1]>$maxPrice) {
		$maxChosen = $maxPrice;
		
	} else {
		$maxChosen= $price[1];
	}
} else {
	$minChosen = $minPrice;
	$maxChosen = $maxPrice;
}
$tpl->value("minPrice",$minPrice);
$tpl->value("maxPrice",$maxPrice);
$tpl->value("minChosen",$minChosen);
$tpl->value("maxChosen",$maxChosen);
$priceInterval .= $tpl->build('search/price');

// Partie Nb_people - NbAdults - NbEnfants
$people = "";
if(isset($NbEnfants)) {
	$tpl->value("NbEnfants",$NbEnfants);
} else {
	$tpl->value("NbEnfants",0);
}
if(isset($NbAdults)) {
	$tpl->value("NbAdults",$NbAdults);
} else if(isset($Nb_people) && !isset($NbEnfants)) {
	$tpl->value("NbAdults",$Nb_people);
} else {
	$tpl->value("NbAdults",0);
}
$people = $tpl->build('search/people');

// Partie Owner
$list_owners="";
$others_owners="";
$owners="";
if(isset($ownerUtility)) {
	arsort($ownerUtility);
	$param = "";
	foreach($ownerUtility as $elem) {
		if($param) {$param .=',';}
		$param .= ' ?';
	}
	
	$req = Database::getInstance()->prepare('SELECT * FROM Company WHERE id IN ('.$param.')');
	try {
		$req->execute(array_keys($ownerUtility));
	} catch (Exception $e) {
		printR($e->getMessage().'<br/>SELECT * FROM Company WHERE id IN ('.$param.')');
	}
	while($donnees = $req->fetch()){
		$ownerUtility[$donnees['id']]=array($ownerUtility[$donnees['id']],new Company($donnees));
	}
	foreach ($ownerUtility as $key => $elem) {
		if(is_array($elem)) {
			$tpl->value('word_filtre',$elem[1]->getName());
			if($elem[0]==0) {
				$tpl->value('value',$key);
				$others_owners .= $tpl->build('search/owners/sub_others');
			} else {
				if($elem[0]%2==0) {
					$tpl->value('sign_filtre',"plus");
					$tpl->value('value',$key);
				} else {
					$tpl->value('sign_filtre',"minus");
					$tpl->value('value',"-".$key);
				}
				if($elem[0]>2) {
					$tpl->value('type_filtre',"default");
				} else if($elem[0]==2) {
					$tpl->value('type_filtre',"success");
				} else {
					$tpl->value('type_filtre',"danger");
				}
				$tpl->value('score',$elem[0]);
				$list_owners .= $tpl->build('search/owners/element');
			}
		}
	}
	$tpl->value('list_others',$others_owners);
	$others_owners = $tpl->build('search/owners/others');
	if($others_owners && !$list_owners) {
		$list_owners = "Choisir une société ?";
	}
	$tpl->value('list_owners',$list_owners);
	$tpl->value('others_owners',$others_owners);
	$owners = $tpl->build('search/owners/owners');
}



// Ensemble de la recherche avancée
$tpl->value('filtre',$filtre);
$tpl->value('categorie',$categorie);
$tpl->value('disponibility',$disponibility);
$tpl->value('advice',$stars);
$tpl->value('priceInterval',$priceInterval);
$tpl->value('people',$people);
$tpl->value('owners',$owners);
$advanced_search .= $tpl->build('search/advanced_search');


// Page Courante
$pageCourante="";
if(!isset($currentPage)) {$currentPage=1;}
$tpl->value('currentPage',$currentPage);
if(!isset($nb_page)) {$nb_page=1;}
$tpl->value('nb_page',$nb_page);
$tpl->value('total',sizeof($table));
if(!isset($nb_offer)) {$nb_offer=10;}
$tpl->value('nb_offer',$nb_offer);
if(!isset($order)) {$order=0;}
$tpl->value('order',$order);
if(!isset($sens)) {$sens=0;}
if($sens) {$tpl->value('downUp',"down");} else {$tpl->value('downUp',"up");}
$tpl->value('sens',$sens);
$pageCourante .= $tpl->build('search/page');
/* Bug Contenu du panier
$panier = $user->getBag();
$boucle = "";
foreach ($panier as $reserv) {
	$service = DBH::getUnique("Service",$arrayName = array('id' => $reserv->getConcern()));
	$tpl->value('name',$service->getName());
	$tpl->value('price',$service->getPrice());
	$boucle .= $tpl->build('reservation/smallact');
}
$tpl->value('boucle_panier',$boucle);
*/

$tpl->value('notif','');

if(!empty($notifSuccess))
{
	$tpl->value('messageSuccess',$notifSuccess);
	$notifs.=$tpl->build('notif/success');
}
if(!empty($notifErreur))
{
	$tpl->value('messageErreur',$notifErreur);
	$notifs.=$tpl->build('notif/erreur');
}
$tpl->value('notif',$notifs);



$tpl->value('pageCourante',$pageCourante);
$tpl->value('advanced_search', $advanced_search);
$tpl->value('boucle_services', $data);
$tpl->value('url','search');
$page.=$tpl->build('search/search');
