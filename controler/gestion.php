<?php
/**
* @author Leboc Philippe
* @version 1.0
*/
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

$submitted_edit = false;

if(!empty($user))
{
	if(!empty($_POST['lastname']) && !empty($_POST['firstname']) && !empty($_POST['mail']) && !empty($_POST['address']) && !empty($_POST['phone'])){
		/////
		/////	MISE A JOUR DES DONNEES DE L'UTILISATEUR
		/////
		$continue = true;
		$updatePassword = false;
		$submitted_edit = true;

		$lastname = htmlspecialchars($_POST['lastname']);
		$firstname = htmlspecialchars($_POST['firstname']);
		$mail = htmlspecialchars($_POST['mail']);
		$address = htmlspecialchars($_POST['address']);
		$phone = htmlspecialchars($_POST['phone']);
		$old_password = NULL;
		$new_password = NULL;

		// validation check require
		$continue = validate_string($lastname) && validate_string($firstname) && validate_string($address, 10, 75) && validate_mail($mail) && validate_phone($phone);
		// end validation

		if($continue && (!empty($_POST['old_password']) && !empty($_POST['new_password']) && !empty($_POST['confirm_new_password']))){
			// Check que l'utilisateur ne se soit pas trompé dans la confirmation du mot de passe.
			if($_POST['new_password'] === $_POST['confirm_new_password']){
				$old_password = htmlspecialchars($_POST['old_password']);
				$new_password = htmlspecialchars($_POST['new_password']);

				// JE NE FAIS PAS DE validate_password() CAR CELA DEPENDRA DE L'ENCODAGE DU MOT DE PASSE.

				// check de l'ancien mot de passe
				if($old_password === $user->getPassword()){
					// Tout est good pour le mot de passe.
					// Ceci permet de signaler que la modification du mot de passe sera necessaire.
					$updatePassword = true;
				}else{
					$notifErreur=_("Le mot de passe saisi ne correspond pas à votre mot de passe !");
					$continue = false;
				}
			}else{
				$notifErreur=_("La confirmation du nouveau mot de passe et le nouveau mot de passe ne sont pas identiques !");
				$continue = false;
			}
		}

		// Si et seulement s'il n'y a eu aucun soucis
		if($continue){
			// Mise à jour des informations de l'utilisateur
			$user->setName($lastname);
			$user->setFirstname($firstname);
			$user->setMail($mail);
			$user->setPhone($phone);
			$user->setAddress($address);		

			if($updatePassword){
				// Mise à jour du mot de passe
				$user->setPassword($new_password);
			}

			if(DBH::save($user)){
				$notifSuccess=_("Vos informations ont bien été mises à jour !");
			}else{
				$notifErreur=_("Une erreur s'est produite lors de la mise à jour de vos données. Contactez nous ou réessayer ultérieurement.");
			}
		}else{
			// nothing
		}
	}else{
		// Page par défaut lors de l'arrivée dans la gestion.
	}
}else{
	// access denied
	header("Location: index.php");
}

// SYSTEM DE NOTIFICATION
$notifs = NULL;

if(!empty($notifSuccess)){
	$tpl->value('messageSuccess',$notifSuccess);
	$notifs .= $tpl->build('notif/success');
}

if(!empty($notifErreur)){
	$tpl->value('messageErreur',$notifErreur);
	$notifs .= $tpl->build('notif/erreur');
}

$tpl->value('notif',$notifs);

// FIN SYSTEM DE NOTIFICATION
$tpl->value('url','gestion');
if((!empty($_GET['button']) && $_GET['button']==='user-edit') || $submitted_edit){
	$tpl->value('UserFirstname',$user->getFirstname());
	$tpl->value('UserLastname',$user->getName());
	$tpl->value('UserMail',$user->getMail());
	$tpl->value('UserAddress',$user->getAddress());
	$tpl->value('UserPhone',$user->getPhone());
	$page.=$tpl->build('user_panel/user_edit');
}else{
	$tpl->value('userFirstname',$user->getFirstname());
	$tpl->value('userLastName',$user->getName());
	$page.=$tpl->build('user_panel/client_gestion');
}