<?php
/**
* @author Leboc Philippe
* @version 1.0
*/
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

if(isset($_POST['inscription']))
{
	extract($_POST);
	if(!validate_string($firstname))
	{
		$erreurs[] = _("Merci d'indiquer votre prénom (de 2 à 16 caractères)");
	}
	if(!validate_string($name))
	{
		$erreurs[] = _("Merci d'indiquer votre nom (de 2 à 16 caractères)");
	}
	if(!validate_phone($phone))
	{
		// numéro français format 00336xxxxxxxx ou 06xxxxxxxx
		$erreurs[] = _("Merci d'indiquer votre numéro de téléphone (de 10 à 13 caractères)");
	}
	if(!validate_string($address, 5, 255))
	{
		// un minimum de caractères permet de "confirmer" que l'utilisateur indique une adresse "potable"
		$erreurs[] = _("Merci d'indiquer votre adresse (minimum 5 caractères)");
	}
	if(empty($password))
	{
		// pas de check sur la longueur du password du fait qu'il
		// sera hash avant d'arriver ici (javascript) il aura donc
		// forcément une certaine longueur.
		$erreurs[] = _("Merci d'indiquer un mot de passe");
	}
	if(empty($password2))
	{
		$erreurs[] = _("Merci de confirmer le mot de passe");
	}
	if(!validate_mail($mail))
	{
		$erreurs[] = _("Merci d'indiquer une adresse mail valide : client@exemple.com");
	}
	if(!validate_mail($mail2))
	{
		$erreurs[] = _("Merci de confirmer l'adresse mail valide : client@exemple.com");
	}

	if(empty($erreurs))
	{
		if($password !== $password2)
		{
			$erreurs[] = _("Les deux mots de passe ne concordent pas");
		}
		if($mail !== $mail2)
		{
			$erreurs[] = _("Les adresses mails ne concordent pas");
		}

		$id = array("mail" => $mail);
		$user = DBH::getUnique('User',$id);

		if(!empty($user))
		{
			$erreurs[] = _("Cette adresse mail est déjà utilisée");
		}
		$user = NULL;
	}

	if(empty($erreurs))
	{
		$user = DBH::create('User',$_POST);
		DBH::save($user);
		$user = DBH::getUnique('User',array(
			"mail" => $mail
			));
	}

	foreach ($_POST as $key => $value) {
		$tpl->value($key,$value);
	}
}

$erreurAfficher = '';

if(!empty($erreurs))
{
	foreach ($erreurs as $erreur){
		$tpl->value('messageErreur',$erreur);
		$erreurAfficher .= $tpl->build('notif/erreur');
	}
}

$tpl->value('erreur',$erreurAfficher);

if(empty($user))
{
	$tpl->value('formulaire',$tpl->build('authentification/formulaire_inscription'));
	$tpl->value('boutonCommencer','');
}else{
	$_SESSION['id']=$user->getId();
	$tpl->value('messageSuccess',_('Félicitation, votre compte a été créé, vous pouvez désormais commencer à programmer vos vacances !'));
	$tpl->value('boutonCommencer',$tpl->build('authentification/small/buttonBegin'));
	$tpl->value('formulaire',$tpl->build('notif/success'));
}

$tpl->value('url','inscription');
$page.=$tpl->build('authentification/inscription');