<?php
/**
* @author = Jayet Jules
* @version 1.0
* @Descr = Affiche le détail d'une offre.
*/

// Ne peut être appelé que par le controleur frontal.
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

// Vérification de la présence du paramètre.
if(isset($_GET['service_id']))
{
	// Récupération des informations relatives au service.
	$service=DBH::getUnique("Service",array("id"=>$_GET['service_id']));

	// Récupération du type de service associé.
	$type_service=DBH::getUnique("TypeService",array("id"=>$service->getOf_Type()));

	// Récupération de l'entreprise associée.
	$company=DBH::getUnique("Company",array("id"=>$service->getLinked()));

	// Récupération des commentaires associés à l'offre.
	$advices=DBH::getList('Advice',array("Service"=>$service->getId()));
	
	// Ajout à la page du nom du type de service.
	$tpl->value('type_service_name',$type_service->getName());
	
	// Ajout à la page du nom du service.
	$tpl->value('service_name',$service->getName());
	
	// Ajout à la page de la description du service.
	$tpl->value('service_description',$service->getDescription());
	
	// Ajout de la page de la disponibilité du service.
	if($service->getAvailability()==1) $service_availability='Disponible <span class="glyphicon glyphicon-ok"></span>';
	else $service_availability='Non disponible <span class="glyphicon glyphicon-remove"></span>';
	$tpl->value('service_availability',$service_availability);
	
	// Ajout à la page du prix du service.
	$tpl->value('service_price',$service->getPrice());

	// Ajout à la page du type de service (id).
	$tpl->value('service_type_service',$service->getOf_Type());

	// Ajout à la page de l'id de l'entreprise.
	$tpl->value('company_id',$company->getId());
	
	// Ajout à la page de la description de l'entreprise.
	$tpl->value('company_description',$company->getDescription());
	
	// Ajout à la page de l'image de l'entreprise.
	$tpl->value('company_picture',$company->getPicture());
	
	// Ajout à la pagee du site de l'entreprise.
	$tpl->value('company_website',$company->getWebsite());

	// Ajout à la page de l'url de la page précedente.
	if(!empty($_SERVER["HTTP_REFERER"]))
	$tpl->value('return',$_SERVER["HTTP_REFERER"]); 
	else
	$tpl->value('return',''); 
	// Ajout à la page des commentaires (afficher les commentaires existants).
	$data_advices="";
	foreach($advices as $value){

		// Récupération du nom de l'utilisateur ayant déposé un avis.
		$current_user=DBH::getUnique("user",array("id"=>$value->getUser()));

		// Récupération de la note du commentaire.
		$mark=$value->getMark();

		// Récupération du bon nombre d'étoiles.
		$mark_stars='';
		for($i=0;$i<$mark;$i++){
			$mark_stars.='<span class="glyphicon glyphicon-star"></span>';
		}
		for($j=5;$j>$mark;$j--){
			$mark_stars.='<span class="glyphicon glyphicon-star-empty"></span>';
		}

		// Ajout du commentaire courant aux autres.
		$tpl->value('mark_stars',$mark_stars);
		$tpl->value('name_user',$current_user->getFirstname()." ".$current_user->getName());
		$tpl->value('date_comment',$value->getDate());
		$tpl->value('critical',$value->getCritical());
		$data_advices.=$tpl->build('offre/comment_offre');
	}
	if(!$data_advices) $data_advices='<li class="list-group-item"><p>Pas encore de commentaires pour ce service.</p></li>';
	$tpl->value('service_advices',$data_advices);

	// Ajout à la page des commentaires (créer un nouveau commentaire).
	if(!empty($user)) {
		$add_advice=$tpl->build('offre/addComment_offre');
	} else {
		$add_advice='<p>Vous devez vous connecter pour ajouter un commentaire.</p>';
	}
	$tpl->value('add_advice',$add_advice);

	$tpl->value('id_service',$service->getId());

	// Construction de la page.
	$tpl->value('url','detail_offre');
	$page.=$tpl->build('offre/detail_offre');

}

?>