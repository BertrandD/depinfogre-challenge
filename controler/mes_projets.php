<?php
/**
* @author Louguet Amandine
* @version 2.0
* @descr contrôleur générique pour toute la procédure de réservation (mettre au panier).
*/
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

if(!empty($_GET['action']) && $_GET['action']=='suppr' && !empty($_GET['id'])){
	$projectToBeDeleted= DBH::getUnique('Journey',
			array("user_id"=>$user->getId(), "id" => $_GET['id']));
		if(!empty($projectToBeDeleted)){
			DBH::removeObject($projectToBeDeleted);
		}
		else{
			$tpl->value('messageErreur', "Vous essayez de supprimer un projet inexistant !");
			$page.=$tpl->build('motif/erreur');
		}
}

if(!empty($user)){
	//Reccupération des projets associés à l'utilisateur
	$projets= DBH::getList('Journey',array("user_id" => $user->getId()));

	if(!empty($projets)){
		//affichage des dif projets ainsi que de leurs dates de début & fin
		$listpage = NULL;
		foreach ($projets as $projet) {
			$tpl->value('idSejour', $projet->getId());
			$tpl->value('nomSejour', $projet->getName());
			$tpl->value('dateDeb', $projet->getStart());
			$tpl->value('dateFin', $projet->getEnd());
			$tpl->value('lieu', $projet->getPlace());	
			$tpl->value('nbPart', $projet->getNb_people());
			$tpl->value('userId', $user->getId());
			
			$listpage .= $tpl->build('listProjets');
		}
		$tpl->value('boucle', $listpage);	
		$page.=$tpl->build('mes_projets');
		//$tpl->value('mes_projets',$liste);
	}
	else{
		echo "error";
	}	


}
else{		
	// Si l'utilisateur n'est pas censé se trouver sur cette page
	// un message spécifique lui est affiché et le contenu est caché.
	$notif[] = _("Vous devez être connecté pour accéder à cet espace.");
	$tpl->value('messageErreur', $notif[0]);
	$notification = $tpl->build('notif/erreur');
	$page .= $tpl->value('notifications', $notification);
	$page .= $tpl->build('access_denied');
}