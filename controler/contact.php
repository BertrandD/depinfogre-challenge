<?php
/**
* @author Leboc Philippe
* @version 1.0
*/

if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

$tpl->value("h1",_("Nous contacter"));
$tpl->value('url','contact');
$page.=$tpl->build('info_others/contact');