<?php
/**
* @author = Jayet Jules
* @version 1.0
* @Descr = Enregistre un nouveau commentaire.
*/

// Test de la présence des variables nécessaires.
if(isset($_GET['id_advice']) && isset($_POST['mark']) && isset($_POST['critical'])){

	// Affichage d'un gif de chargement.
	echo '<img src="/ressources/images/loading.gif" />';

	// Récupération de l'id de l'utlisateur.
	$id_user=$user->getId();
	
	// Récupération de l'id du service.
	$id_service=$_GET['id_advice'];

	// Récupération de la critique.
	$critical=$_POST['critical'];
	
	// Récupération de la note.
	$mark=$_POST['mark'];

	// Récupération de la date courante.
	$date=date("Y/m/d");

	// Pour l'instant tous les commentaires sont légitimes.
	$legitimate=true;

	// Construction d'un nouveau commentaire.
	$advice=new Advice(array('user'=>$id_user,'service'=>$id_service,'critical'=>$critical,'mark'=>$mark,'date'=>$date,'legitimate'=>$legitimate));

	// Enregistrement du commentaire dans la bdd.
	DBH::save($advice);

}

?>