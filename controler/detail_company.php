<?php
/**
* @author = Jayet Jules
* @version 1.0
* @Descr = Affiche le détail d'une entreprise.
*/

// Ne peut être appelé que par le controleur frontal.
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

// Vérification de la présence du paramètre.
if(isset($_GET['company_id']))
{

	// Récupération des informations relatives à l'entreprise.
	$company=DBH::getUnique("Company",array("id"=>$_GET['company_id']));

	// Récupération de tous les services.
	$services=DBH::getList('Service',array("Linked"=>$company->getId()));

	// Ajout à la page de l'url de la page précedente.
	$tpl->value('return',$_SERVER["HTTP_REFERER"]); 

	// Parcours de tous les services.
	$data_services="";
	foreach($services as $value){

		// Id du service.
		$id=$value->getId();

		// Nom du service.
		$name=$value->getName();

		// Ajout à la liste des autres services.
		$tpl->value('service_id',$id);
		$tpl->value('service_name',$name);
		$data_services.=$tpl->build('company/service_company');

	}
	if(!$data_services) $data_services='<li class="list-group-item"><p>Pas de service pour cette entreprise.</p></li>';
	$tpl->value('service_advices',$data_services);

	// Ajout à la page du nom de l'entreprise.
	$tpl->value('company_name',$company->getName());

	// Ajout à la page de la description de l'entreprise.
	$tpl->value('company_description',$company->getDescription());
	
	// Ajout à la page de l'image de l'entreprise.
	$tpl->value('company_picture',$company->getPicture());
	
	// Ajout à la pagee du site de l'entreprise.
	$tpl->value('company_website',$company->getWebsite());

	// Construction de la page.
	$tpl->value('url','detail_company');
	$page.=$tpl->build('company/detail_company');

}


?>