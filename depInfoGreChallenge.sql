-- phpMyAdmin SQL Dump
-- version 4.3.12
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Lun 13 Avril 2015 à 14:47
-- Version du serveur :  5.5.41-0+wheezy1
-- Version de PHP :  5.4.39-0+deb7u2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `depInfoGreChallenge`
--
CREATE DATABASE IF NOT EXISTS `depInfoGreChallenge` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `depInfoGreChallenge`;

-- --------------------------------------------------------

--
-- Structure de la table `Advice`
--

DROP TABLE IF EXISTS `Advice`;
CREATE TABLE IF NOT EXISTS `Advice` (
  `User` int(11) NOT NULL,
  `Service` int(11) NOT NULL,
  `critical` varchar(255) NOT NULL,
  `mark` int(11) NOT NULL,
  `date` date NOT NULL,
  `legitimate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Company`
--

DROP TABLE IF EXISTS `Company`;
CREATE TABLE IF NOT EXISTS `Company` (
  `id` int(11) NOT NULL,
  `website` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `responsible` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Dispose`
--

DROP TABLE IF EXISTS `Dispose`;
CREATE TABLE IF NOT EXISTS `Dispose` (
  `Reservation` int(11) NOT NULL,
  `Entity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Entity`
--

DROP TABLE IF EXISTS `Entity`;
CREATE TABLE IF NOT EXISTS `Entity` (
  `id` int(11) NOT NULL,
  `availability` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `Service` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Event`
--

DROP TABLE IF EXISTS `Event`;
CREATE TABLE IF NOT EXISTS `Event` (
  `id` int(11) NOT NULL,
  `name` varchar(15) NOT NULL,
  `start` datetime NOT NULL,
  `length` time NOT NULL,
  `description` varchar(255) NOT NULL,
  `Associated_to` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Journey`
--

DROP TABLE IF EXISTS `Journey`;
CREATE TABLE IF NOT EXISTS `Journey` (
  `id` int(11) NOT NULL,
  `name` varchar(15) NOT NULL,
  `start` datetime NOT NULL,
  `length` time NOT NULL,
  `place` varchar(15) NOT NULL,
  `nb_people` int(11) NOT NULL,
  `User` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Reservation`
--

DROP TABLE IF EXISTS `Reservation`;
CREATE TABLE IF NOT EXISTS `Reservation` (
  `id` int(11) NOT NULL,
  `name` varchar(15) NOT NULL,
  `start` datetime NOT NULL,
  `length` time NOT NULL,
  `description` varchar(255) NOT NULL,
  `Journey` int(11) NOT NULL,
  `Concern` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Service`
--

DROP TABLE IF EXISTS `Service`;
CREATE TABLE IF NOT EXISTS `Service` (
  `id` int(11) NOT NULL,
  `booking` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `availability` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `Linked` int(11) NOT NULL,
  `Of_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Type_Service`
--

DROP TABLE IF EXISTS `Type_Service`;
CREATE TABLE IF NOT EXISTS `Type_Service` (
  `id` int(11) NOT NULL,
  `name` varchar(15) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `User`
--

DROP TABLE IF EXISTS `User`;
CREATE TABLE IF NOT EXISTS `User` (
  `id` int(11) NOT NULL,
  `firstname` varchar(15) NOT NULL,
  `name` varchar(15) NOT NULL,
  `mail` varchar(30) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `access_level` int(11) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Advice`
--
ALTER TABLE `Advice`
  ADD KEY `User` (`User`,`Service`), ADD KEY `UserAdvice` (`Service`);

--
-- Index pour la table `Company`
--
ALTER TABLE `Company`
  ADD PRIMARY KEY (`id`), ADD KEY `responsible` (`responsible`);

--
-- Index pour la table `Dispose`
--
ALTER TABLE `Dispose`
  ADD KEY `Reservation` (`Reservation`,`Entity`), ADD KEY `DisposeEntity` (`Entity`);

--
-- Index pour la table `Entity`
--
ALTER TABLE `Entity`
  ADD PRIMARY KEY (`id`), ADD KEY `Service` (`Service`);

--
-- Index pour la table `Event`
--
ALTER TABLE `Event`
  ADD PRIMARY KEY (`id`), ADD KEY `Associated_to` (`Associated_to`);

--
-- Index pour la table `Journey`
--
ALTER TABLE `Journey`
  ADD PRIMARY KEY (`id`), ADD KEY `User` (`User`);

--
-- Index pour la table `Reservation`
--
ALTER TABLE `Reservation`
  ADD PRIMARY KEY (`id`), ADD KEY `Journey` (`Journey`), ADD KEY `Concern` (`Concern`);

--
-- Index pour la table `Service`
--
ALTER TABLE `Service`
  ADD PRIMARY KEY (`id`), ADD KEY `Linked` (`Linked`), ADD KEY `Of_type` (`Of_type`);

--
-- Index pour la table `Type_Service`
--
ALTER TABLE `Type_Service`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `Company`
--
ALTER TABLE `Company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `Entity`
--
ALTER TABLE `Entity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `Event`
--
ALTER TABLE `Event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `Journey`
--
ALTER TABLE `Journey`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `Reservation`
--
ALTER TABLE `Reservation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `Service`
--
ALTER TABLE `Service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `Type_Service`
--
ALTER TABLE `Type_Service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `User`
--
ALTER TABLE `User`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `Advice`
--
ALTER TABLE `Advice`
ADD CONSTRAINT `ServiceAdvice` FOREIGN KEY (`User`) REFERENCES `User` (`id`),
ADD CONSTRAINT `UserAdvice` FOREIGN KEY (`Service`) REFERENCES `Service` (`id`);

--
-- Contraintes pour la table `Company`
--
ALTER TABLE `Company`
ADD CONSTRAINT `Responsible` FOREIGN KEY (`responsible`) REFERENCES `User` (`id`);

--
-- Contraintes pour la table `Dispose`
--
ALTER TABLE `Dispose`
ADD CONSTRAINT `DisposeEntity` FOREIGN KEY (`Entity`) REFERENCES `Entity` (`id`),
ADD CONSTRAINT `DisposeReservation` FOREIGN KEY (`Reservation`) REFERENCES `Reservation` (`id`);

--
-- Contraintes pour la table `Entity`
--
ALTER TABLE `Entity`
ADD CONSTRAINT `Entity_ibfk_1` FOREIGN KEY (`Service`) REFERENCES `Service` (`id`);

--
-- Contraintes pour la table `Event`
--
ALTER TABLE `Event`
ADD CONSTRAINT `EventReservation` FOREIGN KEY (`Associated_to`) REFERENCES `Reservation` (`id`);

--
-- Contraintes pour la table `Journey`
--
ALTER TABLE `Journey`
ADD CONSTRAINT `UserJourney` FOREIGN KEY (`User`) REFERENCES `User` (`id`);

--
-- Contraintes pour la table `Reservation`
--
ALTER TABLE `Reservation`
ADD CONSTRAINT `ReservationConcern` FOREIGN KEY (`Concern`) REFERENCES `Service` (`id`),
ADD CONSTRAINT `ReservationJourney` FOREIGN KEY (`Journey`) REFERENCES `Journey` (`id`);

--
-- Contraintes pour la table `Service`
--
ALTER TABLE `Service`
ADD CONSTRAINT `ServiceOfType` FOREIGN KEY (`Of_type`) REFERENCES `Type_Service` (`id`),
ADD CONSTRAINT `SerciceCompany` FOREIGN KEY (`Linked`) REFERENCES `Company` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
