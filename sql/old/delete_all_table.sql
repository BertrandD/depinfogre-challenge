#########################################
# @author Leboc Philippe				#
#			DELETE ALL TABLE			#
#########################################
DROP TABLE IF EXISTS `Dispose`;
DROP TABLE IF EXISTS `Entity`;
DROP TABLE IF EXISTS `Event`;
DROP TABLE IF EXISTS `Advice`;
DROP TABLE IF EXISTS `Reservation`;
DROP TABLE IF EXISTS `Service`;
DROP TABLE IF EXISTS `Journey`;
DROP TABLE IF EXISTS `Company`;
DROP TABLE IF EXISTS `Type_Service`;
DROP TABLE IF EXISTS `User`;


