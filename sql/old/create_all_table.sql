#########################################
# @author Leboc Philippe		        		#
#	CREATE ALL TABLE IF NOT EXISTS		    #
#########################################

CREATE TABLE IF NOT EXISTS `User` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(16) NOT NULL,
  `name` varchar(16) NOT NULL,
  `mail` varchar(30) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(13) NOT NULL,
  `access_level` int(3) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `Type_Service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(16) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `Company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `website` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `responsible` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `responsible` (`responsible`),
  CONSTRAINT `Responsible` FOREIGN KEY (`responsible`) REFERENCES `User` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `Journey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(16) NOT NULL,
  `start` datetime NOT NULL,
  `length` time NOT NULL,
  `place` varchar(16) NOT NULL,
  `nb_people` int(11) NOT NULL,
  `User` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `User` (`User`),
  CONSTRAINT `UserJourney` FOREIGN KEY (`User`) REFERENCES `User` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `Service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking` int(11) NOT NULL,
  `name` varchar(16) NOT NULL,
  `description` varchar(255) NOT NULL,
  `availability` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `Linked` int(11) NOT NULL,
  `Of_type` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Linked` (`Linked`),
  KEY `Of_type` (`Of_type`),
  CONSTRAINT `SerciceCompany` FOREIGN KEY (`Linked`) REFERENCES `Company` (`id`),
  CONSTRAINT `ServiceOfType` FOREIGN KEY (`Of_type`) REFERENCES `Type_Service` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `Reservation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(16) NOT NULL,
  `start` datetime NOT NULL,
  `length` time NOT NULL,
  `description` varchar(255) NOT NULL,
  `Journey` int(11) NOT NULL,
  `Concern` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Journey` (`Journey`),
  KEY `Concern` (`Concern`),
  CONSTRAINT `ReservationConcern` FOREIGN KEY (`Concern`) REFERENCES `Service` (`id`),
  CONSTRAINT `ReservationJourney` FOREIGN KEY (`Journey`) REFERENCES `Journey` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `Advice` (
  `User` int(11) NOT NULL,
  `Service` int(11) NOT NULL,
  `critical` varchar(255) NOT NULL,
  `mark` int(11) NOT NULL,
  `date` date NOT NULL,
  `legitimate` int(11) NOT NULL,
  KEY `User` (`User`,`Service`),
  KEY `UserAdvice` (`Service`),
  CONSTRAINT `ServiceAdvice` FOREIGN KEY (`User`) REFERENCES `User` (`id`),
  CONSTRAINT `UserAdvice` FOREIGN KEY (`Service`) REFERENCES `Service` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `Entity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `availability` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `Service` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Service` (`Service`),
  CONSTRAINT `Entity_ibfk_1` FOREIGN KEY (`Service`) REFERENCES `Service` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `Dispose` (
  `Reservation` int(11) NOT NULL,
  `Entity` int(11) NOT NULL,
  KEY `Reservation` (`Reservation`,`Entity`),
  KEY `DisposeEntity` (`Entity`),
  CONSTRAINT `DisposeEntity` FOREIGN KEY (`Entity`) REFERENCES `Entity` (`id`),
  CONSTRAINT `DisposeReservation` FOREIGN KEY (`Reservation`) REFERENCES `Reservation` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `Event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(16) NOT NULL,
  `start` datetime NOT NULL,
  `length` time NOT NULL,
  `description` varchar(255) NOT NULL,
  `Associated_to` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Associated_to` (`Associated_to`),
  CONSTRAINT `EventReservation` FOREIGN KEY (`Associated_to`) REFERENCES `Reservation` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;