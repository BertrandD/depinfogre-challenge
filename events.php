<?php
session_start();
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(E_ALL | E_STRICT);
define("FRONT_CONTROLER", "Yes I'm coming from front controler !");
define('PROJECT_DIR', realpath('./'));
define("CLASS_DIR", PROJECT_DIR."/model/classes/");
define("TPL_DIR", PROJECT_DIR."/templates/");
define("MODEL_DIR", PROJECT_DIR."/model/");

require_once CLASS_DIR.'Database.class.php';
require_once CLASS_DIR.'DBH.class.php';
require_once CLASS_DIR.'Object.class.php';
require_once CLASS_DIR.'User.class.php';
require_once CLASS_DIR.'Service.class.php';
require_once CLASS_DIR.'Reservation.class.php';
require_once CLASS_DIR.'Journey.class.php';
require_once CLASS_DIR.'Advice.class.php';
require_once CLASS_DIR.'Templates.class.php';
require_once MODEL_DIR.'functions.php';

$tpl = new Templates;
$tpl->DEBUG = false; // Pour afficher les {{message}} qui ne sont pas remplacés par le code php, changer en true
$page = '';

if(isset($_SESSION['id']))
{
	$identifier=array(
		'id' => $_SESSION['id']
		);
	$user=DBH::getUnique('User',$identifier);
}

if(!empty($user))
{
	if(!empty($_GET['addServiceToBag']))
	{
		$idService = $_GET['addServiceToBag'];
		if(!empty($idService))
		{
			$serviceToBag = DBH::getUnique('Service',array("id"=>$idService));
			if(!empty($serviceToBag))
			{
				$journey = $user->getNextJourney();
				$params = array('concern' => $idService,'userId' => $user->getId(), 'journey' => $journey->getId()); //@TODO : changer le Journey !!!!!!  
				$reserv = DBH::getUnique('Reservation',$params);
				if(empty($reserv))
				{
					$reservation = DBH::create('Reservation',$params);
					if(!empty($reservation))
					{
						$res = DBH::save($reservation);
						echo "1";
					}
				}else{
					echo "0";
				}
			}
		}
	}

	if(!empty($_GET['getCalendar']))
	{
		// liste des événements
		$json = array();

 		// exécution de la requête
		$q = Database::getInstance()->prepare("SELECT * FROM `agenda` WHERE UserId=?");
		$q->execute(array($user->getId()));
		 // envoi du résultat au success
		echo json_encode($q->fetchAll(PDO::FETCH_ASSOC));
	}

	if(!empty($_GET['getBagNotInCalendar']))
	{
		$reservationsWithoutDate = DBH::getList('Reservation',
			array(
				'UserId'=>$user->getId(),
				'start'=>'0000-00-00 00:00:00',
				'end'=>'0000-00-00 00:00:00'
				),
			array(),
			'*',
			array(
				'UserId'=>$user->getId(),
				'start'=>'null',
				'end'=>'null'
				)
			);

		$reservationTpl='';
		foreach ($reservationsWithoutDate as $reservation) {
			$tpl->value('id',$reservation->getId());
			$tpl->value('name',$reservation->getName());
			$tpl->value('title',$reservation->getName());
			$serv=$reservation->getService();
			if($serv!=NULL)
				$tpl->value('ServiceName',$serv->getName());
			$reservationTpl.=$tpl->build('recap/small/activityUnique');
		}

		echo $reservationTpl;
	}

	if(!empty($_GET['update']))
	{
		switch ($_GET['update']) {
			case 1:
			if(!empty($_POST['id']))
			{
				extract($_POST);

				$sql = "UPDATE Reservation SET start=?, end=? WHERE id=? AND UserId=?";
				$q = Database::getInstance()->prepare($sql);
				$q->execute(array($start,$end,$id,$user->getId()));	
				echo "OK";
			}
			break;
			case 2:
			if(!empty($_POST['id']))
			{
				extract($_POST);

				$sql = "UPDATE Reservation SET start=?, end=? WHERE id=? AND UserId=?";
				$q = Database::getInstance()->prepare($sql);
				$q->execute(array($start,$end,$id,$user->getId()));	
			}
			break;
			case 3:
			if(!empty($_POST['id']))
			{
				extract($_POST);
				$sql = "UPDATE Reservation SET name=? WHERE id=? AND UserId=?";
				$q = Database::getInstance()->prepare($sql);
				$q->execute(array($value,$id,$user->getId()));	
				echo $value;
			}
			break;	
			case 4:
			if(!empty($_POST['title']))
			{
				extract($_POST);
				$sql = "INSERT INTO Reservation(name,start,end,UserId) VALUES(?,?,?,?)";
				$q = Database::getInstance()->prepare($sql);
				$q->execute(array($title,$start,$end,$user->getId()));	
				$q = Database::getInstance()->prepare('SELECT max(id) AS id FROM Reservation WHERE name=? AND start=? AND end=? AND UserId=?');
				$q->execute(array($title,$start,$end,$user->getId()));
				$data = $q->fetch();
				echo $data['id'];
			}
			break;	
			case 5:
			if(!empty($_POST['id']))
			{
				extract($_POST);
				$sql = "UPDATE Reservation SET start=NULL, end=NULL WHERE id=? AND UserId=?";
				$q = Database::getInstance()->prepare($sql);
				$q->execute(array($id,$user->getId()));	
			}
			break;		
			default:
			break;
		}
	}

	if(!empty($_GET['addAdvice'])) {
		if(!empty($_GET['critical']) && !empty($_GET['mark'])) {
			$params = array('User' => $user->getId(), 'Service' => $_GET['addAdvice'], 'critical' => $_GET['critical'], 'mark' => $_GET['mark'], 'date' => date("Y-m-d"), 'legitimate' => 1);
			$req = Database::getInstance()->prepare('SELECT T.id, T.name FROM Type_Service T');
			
			$advice = DBH::create('Advice',$params);
			if(!$advice) {
				echo "0";
			} else {
				DBH::save($advice);
				$mark_stars="";
				for($i=0;$i<$advice->getMark();$i++){
					$mark_stars.='<span class="glyphicon glyphicon-star"></span>';
				}
				for($j=5;$j>$advice->getMark();$j--){
					$mark_stars.='<span class="glyphicon glyphicon-star-empty"></span>';
				}
				$tpl->value('mark_stars',$mark_stars);
				$tpl->value('name_user',$user->getFirstname()." ".$user->getName());
				$tpl->value('date_comment',$advice->getDate());
				$tpl->value('critical',$advice->getCritical());
				$page.=$tpl->build('offre/comment_offre');
				echo $page;
			}
		} else {
			echo "0";
		}
	}
}
?>