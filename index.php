<?php
header('Content-Type: text/html; charset=utf-8');
session_start();

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(E_ALL | E_STRICT);

define('PROJECT_DIR', realpath('./'));
define("FRONT_CONTROLER", "Yes I'm coming from front controler !");
define("MODEL_DIR", PROJECT_DIR."/model/");
define("CLASS_DIR", PROJECT_DIR."/model/classes/");
define("ENUM_DIR", PROJECT_DIR."/model/classes/enums/");
define("EXCEPTION_DIR", PROJECT_DIR."/model/classes/exception/");
define("CONTROLER_DIR", PROJECT_DIR."/controler/");
define("TPL_DIR", PROJECT_DIR."/templates/");
define("UPLOAD_DIR", PROJECT_DIR."/ressources/images/uploads/");
define("LIB_DIR", PROJECT_DIR."/lib/");

define('LOCALE_DIR', PROJECT_DIR.'/locale');
define('DEFAULT_LOCALE', 'fr_FR');

require_once EXCEPTION_DIR.'FrontControlerException.class.php';
require_once EXCEPTION_DIR.'NotSupportedMethodException.class.php';

require_once ENUM_DIR.'AccessLevel.enum.php';

require_once CLASS_DIR.'Database.class.php';
require_once CLASS_DIR.'DBH.class.php';
require_once CLASS_DIR.'Object.class.php';
require_once CLASS_DIR.'User.class.php';
require_once CLASS_DIR.'Service.class.php';
require_once CLASS_DIR.'TypeService.class.php';
require_once CLASS_DIR.'Company.class.php';
require_once CLASS_DIR.'Journey.class.php';
require_once CLASS_DIR.'Reservation.class.php';
require_once CLASS_DIR.'Advice.class.php';
require_once CLASS_DIR.'Templates.class.php';

require_once MODEL_DIR.'functions.php';

require_once LIB_DIR.'gettext/gettext.inc';

$supported_locales = array('fr_FR', 'en_US', 'zh_CN');
$encoding = 'UTF-8';

if(isset($_GET['lang']))
{
	$locale = $_GET['lang'];
	setcookie("lang",$locale);
}else if(isset($_COOKIE['lang'])){
	$locale = $_COOKIE['lang'];
}else{
	$locale = DEFAULT_LOCALE;
}
//$locale = (isset($_GET['lang']))? $_GET['lang'] : DEFAULT_LOCALE;

// gettext setup
T_setlocale(LC_MESSAGES, $locale.'.utf8');
// Set the text domain as 'messages'
$domain = 'default';
bindtextdomain($domain, LOCALE_DIR);
// bind_textdomain_codeset is supported only in PHP 4.2.0+
if (function_exists('bind_textdomain_codeset')) 
  bind_textdomain_codeset($domain, $encoding);
textdomain($domain);
$tpl = new Templates;
$tpl->DEBUG = false; // Pour afficher les {{message}} qui ne sont pas remplacés par le code php, changer en true
$page = '';

	include(CONTROLER_DIR."header.php");
	if (isset($_GET['op'])&&(file_exists(CONTROLER_DIR.$_GET['op'].'.php')))
	{
		include(CONTROLER_DIR.$_GET['op'].'.php');
	}
	else
	{
		include(CONTROLER_DIR.'accueil.php');
	}
	
	$tpl->value('lang', $locale);
	$tpl->value('titre', "Alp'Agenda");
	$tpl->value('content',$page);

$generatedPage = $tpl->build('principal');

echo $generatedPage;
