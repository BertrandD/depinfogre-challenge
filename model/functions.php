<?php

function printR($var)
{
	echo "<pre>";
	print_r($var);
	echo "</pre>";
}

/**
* @author Leboc Philippe
* @version 1.0
* @descr Test la conformité des textes provenant essentiellement des champs (ex: input type="text")
*		Une limitation HTML est imposée mais ce check est également obligatoire et primordial !
* @condition !empty($text) && ($min <= $text <= $max) && (is_string($text))
* @return True si le paramètre est jugé conforme aux normes 
*		établies par nos soins concernant le format de données 
*		reçu par un utilisateur. False sinon.
*/
function validate_string($text, $min = 2, $max = 16){
	$res = false;

	// Transforme les caractères htmls en caractères courant
	$text = htmlspecialchars($text);

	// début des check
	if(!empty($text)){
		if(is_string($text) && strlen($text) >= $min && strlen($text) <= $max){
			$res = true;
		}
	}

	return $res;
}

/**
* @author Leboc Philippe
* @version 1.0
* @descr Test la conformité des nombres provenant essentiellement des champs (ex: input type="number")
*		Une limitation HTML est imposée mais ce check est également obligatoire et primordial !
* @condition !empty($number) && (is_numeric($number)) && ($min <= $number <= max)
* @return True si le paramètre est jugé conforme aux normes 
*		établies par nos soins concernant le format de données 
*		reçu par un utilisateur. False sinon.
*/
function validate_numeric($number, $min = 0, $max = 100){
	$res = false;

	// Transforme les caractères htmls en caractères courant
	$number = htmlspecialchars($number);

	// début des check
	// ATTENTION : $var = 0 est considérée comme EMPTY !!!
	if(isset($number)){
		if((is_numeric($number)) && ($number >= $min) && ($number <= $max)){
			$res = true;
		}
	}

	return $res;
}

/**
* @author Leboc Philippe
* @version 1.0
* @descr Test la conformité du champ provenant essentiellement des champs (ex: input type="text")
*		Aucune limitation par le navigateur pour les numéro de téléphone.
* @condition !empty($phone) && $phone est au bon format
* @return True si le paramètre est jugé conforme aux normes 
*		établies par nos soins concernant le format de données 
*		reçu par un utilisateur. False sinon.
*/
function validate_phone($phone){
	$res = false;

	// Transforme les caractères htmls en caractères courant
	$phone = htmlspecialchars($phone);

	// début des check
	if(!empty($phone)){
		if(preg_match('/^[+]?([\d]{0,3})?[\(\.\-\s]?([\d]{3})[\)\.\-\s]*([\d]{3})[\.\-\s]?([\d]{4})$/', $phone)){
			$res = true;
		}
	}

	return $res;
}


/**
* @author Leboc Philippe
* @version 1.0
* @descr Test la conformité de l'adresse e-mail provenant essentiellement des champs (ex: input type="email")
*		Certains navigateurs ne check pas le type email mais dans tous les cas cette vérification est obligatoire.
* @condition !empty($email) && (is_mail($number)) && (7 <= $email <= 30)
* @return True si le paramètre est jugé conforme aux normes 
*		établies par nos soins concernant le format de données 
*		reçu par un utilisateur. False sinon.
*/
function validate_mail($email){
	$res = false;

	// Transforme les caractères htmls en caractères courant
	$email = htmlspecialchars($email);

	// début des check
	if(!empty($email)){
		if(filter_var($email, FILTER_VALIDATE_EMAIL) && (strlen($email) >= 7) && (strlen($email) <= 30)){
			$res = true;
		}
	}

	return $res;
}

/**
* @author Leboc Philippe
* @version 1.0
*/
function validate_file($file, $userId){

	// Utile à savoir :
	//$_FILES['fichier']['name']	 //Le nom original du fichier, comme sur le disque du visiteur (exemple : mon_icone.png).
	//$_FILES['fichier']['type']     //Le type du fichier. Par exemple, cela peut être « image/png ».
	//$_FILES['fichier']['size']     //La taille du fichier en octets.
	//$_FILES['fichier']['tmp_name'] //L'adresse vers le fichier uploadé dans le répertoire temporaire.
	//$_FILES['fichier']['error']    //Le code d'erreur, qui permet de savoir si le fichier a bien été uploadé.

	$continue = TRUE;
	$erreur = NULL;
	$result = NULL;

	// Fixe la taille max en octets
	$maxsize = 51200;
	$maxwidth = 600;
	$maxheight = 600;

	// Fixe les extensions valides
	$extensions_valides = array( 'jpg' , 'jpeg' , 'gif' , 'png' );

	// Recupère l'extension
	$extension_upload = strtolower(substr(strrchr($_FILES['fichier']['name'], '.'), 1));

	if($_FILES['fichier']['error'] > 0){
		// Une erreur est servenue, on s'arrête
		$continue = FALSE;
	}else{
		// Traitement de la taille du fichier (en octets)
		if($continue && ($_FILES['fichier']['size'] > $maxsize)){
			$erreur = "Le fichier est trop gros";
			$continue = FALSE;
		}

		// Traitement de l'extension
		if ($continue && (!in_array($extension_upload,$extensions_valides))){
			$erreur = "Extension incorrecte";
			$continue = FALSE;
		}

		// traitement des dimensions de l'image
		$image_sizes = getimagesize($_FILES['fichier']['tmp_name']);

		if($continue && ($image_sizes[0] > $maxwidth || $image_sizes[1] > $maxheight)){
			$erreur = "Image trop grande";
			$continue = FALSE;
		}

		if($continue){
			$nom = UPLOAD_DIR. $userId . "_" . $_FILES['fichier']['name'];
			$transfert = move_uploaded_file($_FILES['fichier']['tmp_name'], $nom);
			
			if (!$transfert){
				$continue = FALSE;
			}
		}
	}

	if($continue){
		$result = $nom;
	}

	return $result;
}