<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}
/**
* Exception levée quand un script php n'est pas appelée depuis le controleur frontal
*/
class FrontControlerException extends Exception
{
	function __construct()
	{
		parent::__construct(_("Vous ne pouvez pas accéder à ce script directement"));
	}
}