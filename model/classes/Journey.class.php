<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

/**
* @author = Leboc Philippe
* @version 1.0
* @Descr = Un voyage
*/
class Journey extends Object {

	// table
	public static $table = 'Journey';

	// Attributs
	protected $id;	// integer
	protected $name;	// string
	protected $start;	// datetime
	protected $end;	// datetime
	protected $place;	// string
	protected $nb_people;	// integer
	protected $user_id;	// integer

	// constructeur
	public function __construct($data = array()){
		parent::__construct($data);
	}

    public function getIdentity()
    {
      return array(
        "start" =>$this->getStart(),
        "end" => $this->getEnd()
        );
    }

    public function getOwner()
    {
    	return DBH::getUnique('User', array("id" => $this->getUser_id()));
    }


    // Getters & Setters
    public function getId(){
    	return $this->id;
    }

    public function getName(){
    	return $this->name;
    }

    public function getStart(){
    	return $this->start;
    }

    public function getEnd(){
    	return $this->end;
    }

    public function getPlace(){
    	return $this->place;
    }

    public function getNb_people(){
    	return $this->nb_people;
    }

    public function getUser_id(){
    	return $this->user_id;
    }

    public function setId($value){
    	$this->id = $value;
    }

    public function setName($value){
    	$this->name = $value;
    }

    public function setStart($value){
    	$this->start = $value;
    }

    public function setEnd($value){
    	$this->end = $value;
    }

    public function setPlace($value){
    	$this->place = $value;
    }

    public function setNb_people($value){
    	$this->nb_people = $value;
    }

    public function setUser_id($value){
    	$this->user_id = $value;
    }
}