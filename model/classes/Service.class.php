<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

/**
* @Name = Service
* @author = Bertrand Darbon
* @Descr = Sert d'objet Service.
*/
class Service extends Object {
	
	public static $table = 'Service';

	protected $id;
	protected $booking;
	protected $name;
	protected $description;
	protected $availability;
	protected $price;
	protected $linked;
	protected $of_Type;
	protected $nb_people;

	// Constructeur
    public function __construct($data = array()){
        parent::__construct($data);
    }

    public function getIdentity()
    {
      return array(
          "id" =>$this->getId()
        );
    }

    public function getCompany()
    {
    	return DBH::getUnique('Company',
    		array(
    			"id" => $this->getLinked()
    			)
    		);
    }

    public function getType()
    {
    	return DBH::getUnique('TypeService',
    		array(
    			"id" => $this->getOf_Type()
    			)
    		);
    }

	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getBooking(){
		return $this->booking;
	}

	public function setBooking($booking){
		$this->booking = $booking;
	}

	public function getName(){
		return $this->name;
	}

	public function setName($name){
		$this->name = $name;
	}

	public function getDescription(){
		return $this->description;
	}

	public function setDescription($description){
		$this->description = $description;
	}

	public function getAvailability(){
		return $this->availability;
	}

	public function setAvailability($availability){
		$this->availability = $availability;
	}

	public function getPrice(){
		return $this->price;
	}

	public function setPrice($price){
		$this->price = $price;
	}

	public function getLinked(){
		return $this->linked;
	}

	public function setLinked($linked){
		$this->linked = $linked;
	}

	public function getOf_Type(){
		return $this->of_Type;
	}

	public function setOf_Type($of_Type){
		$this->of_Type = $of_Type;
	}

	public function getNb_people(){
		return $this->nb_people;
	}

	public function setNb_people($nb_people){
		$this->nb_people = $nb_people;
	}
}