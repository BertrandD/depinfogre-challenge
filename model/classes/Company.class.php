<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

/**
* @Name = Company
* @author = Bertrand Darbon
* @Descr = Sert d'objet Company.
*/
class Company extends Object {
	
	public static $table = 'Company';

	protected $id;
	protected $name;
	protected $website;
	protected $description;
	protected $picture;
	protected $responsible;
	protected $privatePhone;
	protected $publicPhone;
	protected $publicMail;
	protected $address;

	// Constructeur
    public function __construct($data = array()){
        parent::__construct($data);
    }

    public function getIdentity()
    {
      return array(
          "responsible" =>$this->getResponsible()
        );
    }

    public function getOwner()
    {
    	return DBH::getUnique('User',
    		array(
    			"id" => $this->getResponsible()
    			)
    		);
    }

	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getName(){
		return $this->name;
	}

	public function setName($name){
		$this->name = $name;
	}

	public function getWebsite(){
		return $this->website;
	}

	public function setWebsite($website){
		$this->website = $website;
	}

	public function getDescription(){
		return $this->description;
	}

	public function setDescription($description){
		$this->description = $description;
	}

	public function getPicture(){
		return $this->picture;
	}

	public function setPicture($picture){
		$this->picture = $picture;
	}

	public function getResponsible(){
		return $this->responsible;
	}

	public function setResponsible($responsible){
		$this->responsible = $responsible;
	}

	public function getPrivatePhone(){
		return $this->privatePhone;
	}

	public function setPrivatePhone($privatePhone){
		$this->privatePhone = $privatePhone;
	}

	public function getPublicPhone(){
		return $this->publicPhone;
	}

	public function setPublicPhone($publicPhone){
		$this->publicPhone = $publicPhone;
	}

	public function getPublicMail(){
		return $this->publicMail;
	}

	public function setPublicMail($publicMail){
		$this->publicMail = $publicMail;
	}

	public function getAddress(){
		return $this->address;
	}

	public function setAddress($address){
		$this->address = $address;
	}
}
?>
