<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

/**
* @author Leboc Philippe
* @version 1.0
* @Descr = Définit les droits d'accès
*/
abstract class AccessLevel{
	const USER = 0;
	const COMPANY = 1;
	const ADMIN = 2;

	public static function toString($value){
		$result = NULL;

		switch($value)
		{
			case 0:
				$result = "Utilisateur";
				break;
			case 1:
				$result = "Entreprise";
				break;
			case 2:
				$result = "Administrateur";
				break;
			default:
				break;
		}

		return $result;
	}
}