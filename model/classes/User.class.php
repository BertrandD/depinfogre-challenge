<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

/**
* @Name = User
* @author = Bertrand Darbon
* @Descr = Sert d'objet User.
*/
class User extends Object {
	
	public static $table = 'User';

	protected $id; 			//int
	protected $firstname;	//string
	protected $name;		//string
	protected $mail;		//string
	protected $address;		//string
	protected $phone;		//int
	protected $access_level;//int
	protected $password;	//string
	
	// Constructeur
    public function __construct($data = array()){
        parent::__construct($data);
    }

    public function getIdentity()
    {
      return array(
          "mail" =>$this->getMail()
        );
    }

    public function getNextJourney()
    {
    	return DBH::getUnique('Journey', array(
    			'user_id' => $this->getId(),
    			'end'=> array('>', date('Y-m-d H:i:s',time()))
    		));
    }

    public function getNbBagElements()
    {
    	return DBH::getCount('Reservation', array("UserId" => $this->id, "State" => '0'));
    }

    public function getBag()
    {
		return DBH::getList('Reservation',
			array(
				"UserId" => $this->id, 
				"State" => '0',
				"Concern" => "not null"
				)
			);
    }

	// Getters & Setters
	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getFirstname(){
		return $this->firstname;
	}

	public function setFirstname($firstname){
		$this->firstname = $firstname;
	}

	public function getName(){
		return $this->name;
	}

	public function setName($name){
		$this->name = $name;
	}

	public function getMail(){
		return $this->mail;
	}

	public function setMail($mail){
		$this->mail = $mail;
	}

	public function getAddress(){
		return $this->address;
	}

	public function setAddress($address){
		$this->address = $address;
	}

	public function getPhone(){
		return $this->phone;
	}

	public function setPhone($phone){
		$this->phone = $phone;
	}

	public function getAccess_level(){
		return $this->access_level;
	}

	public function setAccess_level($access_level){
		$this->access_level = $access_level;
	}

	public function getPassword(){
		return $this->password;
	}

	public function setPassword($password){
		$this->password = $password;
	}
}
?>
