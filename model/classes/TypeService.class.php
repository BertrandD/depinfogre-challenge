<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

/**
* @Name = TypeService
* @author = Bertrand Darbon
* @Descr = Sert d'objet TypeService.
*/
class TypeService extends Object {
	
	public static $table = 'Type_Service';

	protected $id;
	protected $name;
	protected $category;
	protected $description;


	// Constructeur
    public function __construct($data = array()){
        parent::__construct($data);
    }

    public function getIdentity()
    {
      return array(
          "name" =>$this->getName()
        );
    }

	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getName(){
		return $this->name;
	}

	public function setName($name){
		$this->name = $name;
	}

	public function getDescription(){
		return $this->description;
	}

	public function setDescription($description){
		$this->description = $description;
	}

	public function getCategory(){
		return $this->category;
	}

	public function setCategory($category){
		$this->category = $category;
	}
}