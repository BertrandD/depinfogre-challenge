<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

/**
* @Name = Advice
* @author = Alexandre Delolme
* @Descr = Sert d'objet Advice.
*/
class Advice extends Object {
	
	public static $table = 'Advice';

	protected $id;
	protected $user;
	protected $service;
	protected $critical;
	protected $mark;
	protected $date;
	protected $legitimate;

	// Constructeur
    public function __construct($data = array()){
        parent::__construct($data);
    }

    public function getIdentity()
    {
      return array(
          "critical" =>$this->getCritical()
        );
    }

    public function getId(){
		return $this->id;
	}

	public function getUser(){
		return $this->user;
	}

	public function setUser($user){
		$this->user = $user;
	}

	public function getCritical(){
		return $this->critical;
	}

	public function setCritical($critical){
		$this->critical = $critical;
	}

	public function getService(){
		return $this->service;
	}

	public function setService($service){
		$this->service = $service;
	}

	public function getMark(){
		return $this->mark;
	}

	public function setMark($mark){
		$this->mark = $mark;
	}

	public function getDate(){
		return $this->date;
	}

	public function setDate($date){
		$this->date = $date;
	}

	public function getLegitimate(){
		return $this->legitimate;
	}

	public function setLegitimate($legitimate){
		$this->legitimate = $legitimate;
	}

}
?>
