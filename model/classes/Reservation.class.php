<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

/**
* @author = Leboc Philippe
* @version 1.0
* @Descr = La panier de l'utilisateur
*/
class Reservation extends Object {

	// table
	public static $table = 'Reservation';

	// Attributs
	protected $id;	// integer
	protected $name;	// string
	protected $start;	// datetime
	protected $length;	// integer
	protected $description;	// string
	protected $journey;	// integer
	protected $concern;	// integer (serviceId)
	protected $userId;	// integer
	protected $state;	// integer : 0 = added to bag, 1 = payment done

	// constructeur
	public function __construct($data = array()){
		parent::__construct($data);
	}

	// Getters Objets
    public function getIdentity()
    {
      return array(
        "journey" =>$this->getJourney(),
        "concern" =>$this->getConcern(),
        "userId" => $this->getUserId()
        );
    }

    public function getOwner()
    {
    	return DBH::getUnique('User', array("id" => $this->getUserId()));
    }

    public function getService(){
        $test = $this->getConcern();
        if(!empty($test)){
    	   return DBH::getUnique('Service', array("id" => $this->getConcern()));
        }
    }

    public function getObjectJourney(){
        return DBH::getUnique('Journey', array('id' => $this->getJourney()));
    }

    // Getters & Setters
    public function getId(){
    	return $this->id;
    }

    public function getName(){
    	return $this->name;
    }

    public function getStartDate(){
    	return $this->start;
    }

    public function getLength(){
    	return $this->length;
    }

    public function getDescription(){
    	return $this->description;
    }

    public function getJourney(){
    	return $this->journey;
    }

	public function getConcern(){
    	return $this->concern;
    }

    public function getUserId(){
    	return $this->userId;
    }

    public function getState(){
    	return $this->state;
    }

    public function setId($value){
    	$this->id = $value;
    }

    public function setName($value){
    	$this->name = $value;
    }

    public function setStart($value){
    	$this->start = $value;
    }

    public function setLength($value){
    	$this->length = $value;
    }

    public function setDescription($value){
    	$this->description = $value;
    }

    public function setJourney($value){
    	$this->journey = $value;
    }

    public function setConcern($value){
    	$this->concern = $value;
    }

    public function setUserId($value){
    	$this->userId = $value;
    }

    public function setState($value){
    	$this->state = $value;
    }
}